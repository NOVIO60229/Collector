﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Warning", menuName = "AttackPattern/Warning")]
public class Warning : AttackPattern
{
    private BossRoom Room;
    public Vector3 aimStartPoint;
    public Vector3 aimDir;
    public float warningBeat;

    public override void Initialize()
    {
        base.Initialize();
    }
    public override void Start()
    {
        Room = BossRoom.Instance;
    }
    public override void Update()
    {
        if (Check())
        {
            Fire();
        }
    }
    public override void End()
    {
    }

    void Fire()
    {
        BossAnimation.Instance.Animator.Play("Alt");
        WarningUtil.Instance.DrawBox(aimStartPoint, aimDir, Room.HorizontalDistance * 2f, 15f, bpm._beatIntervalSec * warningBeat);
    }
}