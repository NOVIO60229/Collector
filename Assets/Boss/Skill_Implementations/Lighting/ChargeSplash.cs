﻿using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ChargeSplash", menuName = "AttackPattern/ChargeSplash")]
public class ChargeSplash : AttackPattern
{
    BossAnimation bAni;
    BossStatus bStatus;
    public float persistBeat;
    public override void Initialize()
    {
        base.Initialize();
        bStatus = BossStatus.instance;
        bAni = BossAnimation.Instance;
    }
    public override void Start()
    {

    }
    public override void Update()
    {
        if (Check())
        {
            bStatus.PlaySplash(persistBeat * 0.46875f);
        }
    }
    public override void End()
    {

    }
}

