﻿using DG.Tweening;
using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GroundHookAttack", menuName = "AttackPattern/GroundHookAttack")]
public class GroundHookAttack : AttackPattern
{
    BossRoom Room;
    BossStatus BStatus;
    public override void Initialize()
    {
        base.Initialize();
        Room = BossRoom.Instance;
        BStatus = BossStatus.instance;
    }
    public override void Update()
    {
        if (Check())
        {
            Room.GroundHook.Play();
            bpm.StartCoroutine(MoveAndPunch());
        }
    }

    IEnumerator MoveAndPunch()
    {
        Vector3 center = Room.RoomPositionDivided(1, 0.5f, 1, 0.5f);
        Vector3 centerBot = Room.RoomPositionDivided(1, 0.5f, 1, 1f);

        BStatus.SetIsFollow(false);

        BStatus.transform.DOMove(center, 6 * 0.468f);
        yield return new WaitForSeconds(4 * 0.468f);

        yield return new WaitForSeconds(0.234f);
        BossAnimation.Instance.Animator.Play("Punch");
        BStatus.transform.DOMove(BStatus.transform.position + Vector3.up, 0.234f);
        yield return new WaitForSeconds(0.468f);
        BStatus.transform.DOMove(Room.PunchPoint1.transform.position, 0.234f);
        yield return new WaitForSeconds(2 * 0.468f);
        BStatus.transform.DOMove(Room.RoomPositionDivided(1, 0.5f, 1, 0.6f), 2 * 0.468f);
        BossAnimation.Instance.Animator.Play("PunchToIdle");

        BStatus.SetIsFollow(true);
    }
}

