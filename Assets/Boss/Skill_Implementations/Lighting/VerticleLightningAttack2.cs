﻿using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "VerticleLightningAttack2", menuName = "AttackPattern/VerticleLightningAttack2")]
public class VerticleLightningAttack2 : AttackPattern
{
    private Vector3[] LeftAttackPoints;
    private Vector3[] RightAttackPoints;
    BossRoom Room;
    public override void Initialize()
    {
        LeftAttackPoints = new Vector3[5];
        RightAttackPoints = new Vector3[5];

        base.Initialize();
        Room = BossRoom.Instance;
        for (int i = 0; i < 5; i++)
        {
            Vector3 point = BossRoom.Instance.RoomPositionDivided(11, 0.5f + i, 1, 1f);
            LeftAttackPoints[i] = point;
        }
        for (int i = 0; i < 5; i++)
        {
            Vector3 point = BossRoom.Instance.RoomPositionDivided(11, 10.5f - i, 1, 1f);
            RightAttackPoints[i] = point;
        }
    }
    public override void Start()
    {

    }
    public override void Update()
    {
        if (Check())
        {
            Warning();
            CameraController.instance.Shake();
        }
    }
    public override void End()
    {

    }

    void Warning()
    {
        bpm.StartCoroutine(DoWarning());
    }
    IEnumerator Cast(Vector3 point, float sec)
    {
        WarningUtil.Instance.DrawBox(point, Vector3.up, Room.VerticalDistance, 5f, sec);
        yield return new WaitForSeconds(sec);
        CastLightning(point);
    }
    IEnumerator DoWarning()
    {
        float interval = BPM_Counter.instance._beatIntervalSec;

        bpm.StartCoroutine(Cast(LeftAttackPoints[0], 2 * interval));
        bpm.StartCoroutine(Cast(LeftAttackPoints[1], 2.5f * interval));
        bpm.StartCoroutine(Cast(LeftAttackPoints[2], 3f * interval));
        bpm.StartCoroutine(Cast(LeftAttackPoints[3], 3.5f * interval));
        bpm.StartCoroutine(Cast(LeftAttackPoints[4], 3.75f * interval));

        yield return new WaitForSeconds(4 * interval);

        bpm.StartCoroutine(Cast(RightAttackPoints[0], 2 * interval));
        bpm.StartCoroutine(Cast(RightAttackPoints[1], 2.5f * interval));
        bpm.StartCoroutine(Cast(RightAttackPoints[2], 3f * interval));
        bpm.StartCoroutine(Cast(RightAttackPoints[3], 3.5f * interval));
        bpm.StartCoroutine(Cast(RightAttackPoints[4], 3.75f * interval));
        yield return new WaitForSeconds(4 * interval);
    }
    void CastLightning(Vector3 point)
    {
        VerticleLighting v = VerticleLighting.Pool.Use().GetComponent<VerticleLighting>();
        v.Initialize(point, 90, 0.468f);
        v.particle.Play();
    }
}

