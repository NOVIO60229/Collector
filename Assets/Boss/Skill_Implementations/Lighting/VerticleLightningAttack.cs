﻿using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "VerticleLightningAttack", menuName = "AttackPattern/VerticleLightningAttack")]
public class VerticleLightningAttack : AttackPattern
{
    private Vector3[] AttackPoints;
    BossRoom Room;
    public override void Initialize()
    {
        AttackPoints = new Vector3[11];
        base.Initialize();
        Room = BossRoom.Instance;
        for (int i = 0; i < 11; i++)
        {
            Vector3 point = BossRoom.Instance.RoomPositionDivided(11, i + 0.5f, 1, 1f);
            AttackPoints[i] = point;
        }
    }
    public override void Start()
    {

    }
    public override void Update()
    {
        if (Check())
        {
            Warning();
            CameraController.instance.Shake();
        }
    }
    public override void End()
    {

    }

    void Warning()
    {
        ani.PlayCharge();
        bpm.StartCoroutine(CastWarning());
    }
    IEnumerator CastWarning()
    {
        float interval = BPM_Counter.instance._beatIntervalSec;

        WarningUtil.Instance.DrawBox(AttackPoints[0], Vector3.up, Room.VerticalDistance, 5f, interval * 3);
        WarningUtil.Instance.DrawBox(AttackPoints[10], Vector3.up, Room.VerticalDistance, 5f, interval * 3);
        yield return new WaitForSeconds(interval);

        WarningUtil.Instance.DrawBox(AttackPoints[2], Vector3.up, Room.VerticalDistance, 5f, interval * 2);
        WarningUtil.Instance.DrawBox(AttackPoints[8], Vector3.up, Room.VerticalDistance, 5f, interval * 2);
        yield return new WaitForSeconds(interval);

        WarningUtil.Instance.DrawBox(AttackPoints[4], Vector3.up, Room.VerticalDistance, 5f, interval);
        WarningUtil.Instance.DrawBox(AttackPoints[6], Vector3.up, Room.VerticalDistance, 5f, interval);
        yield return new WaitForSeconds(interval);

        int[] Indexs = new int[] { 0, 2, 4, 6, 8, 10 };
        foreach (int index in Indexs)
        {
            CastLightning(AttackPoints[index]);
        }

    }
    void CastLightning(Vector3 point)
    {
        ani.EndCharge();
        VerticleLighting v = VerticleLighting.Pool.Use().GetComponent<VerticleLighting>();
        v.Initialize(point, 90, 0.468f);
        v.particle.Play();
    }
}

