﻿using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "VerticleLightningAttack4", menuName = "AttackPattern/VerticleLightningAttack4")]
public class VerticleLightningAttack4 : AttackPattern
{
    private Vector3[] AttackPoints;
    BossRoom Room;
    public override void Initialize()
    {
        AttackPoints = new Vector3[11];
        base.Initialize();
        Room = BossRoom.Instance;
        for (int i = 0; i < 11; i++)
        {
            Vector3 point = BossRoom.Instance.RoomPositionDivided(11, i + 0.5f, 1, 1f);
            AttackPoints[i] = point;
        }
    }
    public override void Start()
    {

    }
    public override void Update()
    {
        if (Check())
        {
            Warning();
        }
    }
    public override void End()
    {

    }

    void Warning()
    {
        bpm.StartCoroutine(DoWarning());
    }
    IEnumerator Cast(Vector3 point, float sec)
    {
        WarningUtil.Instance.DrawBox(point, Vector3.up, Room.VerticalDistance, 5f, sec);
        yield return new WaitForSeconds(sec);
        CastLightning(point);
    }
    IEnumerator DoWarning()
    {
        float interval = BPM_Counter.instance._beatIntervalSec;

        bpm.StartCoroutine(Cast(AttackPoints[0], 2 * interval));
        bpm.StartCoroutine(Cast(AttackPoints[10], 2 * interval));
        bpm.StartCoroutine(Cast(AttackPoints[1], 3f * interval));
        bpm.StartCoroutine(Cast(AttackPoints[9], 3f * interval));
        bpm.StartCoroutine(Cast(AttackPoints[2], 4f * interval));
        bpm.StartCoroutine(Cast(AttackPoints[8], 4f * interval));
        bpm.StartCoroutine(Cast(AttackPoints[3], 6f * interval));
        bpm.StartCoroutine(Cast(AttackPoints[7], 6f * interval));

        yield return new WaitForSeconds(4 * interval);
    }
    void CastLightning(Vector3 point)
    {
        VerticleLighting v = VerticleLighting.Pool.Use().GetComponent<VerticleLighting>();
        v.Initialize(point, 90, 0.468f);
        v.particle.Play();
    }
}

