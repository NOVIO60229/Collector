﻿using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MoveCircle", menuName = "AttackPattern/MoveCircle")]
public class MoveCircle : AttackPattern
{
    BossAnimation bAni;
    BossStatus bStatus;
    public float persistBeat;
    public override void Initialize()
    {
        base.Initialize();
        bStatus = BossStatus.instance;
        bAni = BossAnimation.Instance;
    }
    public override void Start()
    {
    }
    public override void Update()
    {
        if (Check())
        {
            bStatus.PlayCircle(persistBeat * 0.46875f);
            BossMoveTarget.Instance.animator.Play("CirclePattern1");
            bStatus.PlayFly(persistBeat * 0.46875f);
        }
    }
    public override void End()
    {

    }
}

