﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[CreateAssetMenu(fileName = "New LeakElectricity", menuName = "Decoration/LeakElectricity")]
public class LeakElectricity : AttackPattern
{
    public override void Start()
    {
        ani.PlayElectricity();
    }
    public override void Update()
    {
    }
    public override void End()
    {
    }
}
