﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[CreateAssetMenu(fileName = "New StraightMove", menuName = "MovePattern/StraightMove")]
public class StraightMove : AttackPattern
{
    public Vector3 movePoint = Vector3.zero;
    BossStatus BStatus;
    public override void Initialize()
    {
        base.Initialize();
        BStatus = BossStatus.instance;
    }
    public override void Start()
    {
        BStatus.SetIsFollow(false);
        BStatus.transform.DOMove(movePoint, beatFull * bpm._beatIntervalSec);
    }
    public override void Update()
    {
        if (Check())
        {
        }
    }
    public override void End()
    {
        BStatus.SetIsFollow(true);
    }
}
