﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New RotateAroundMove", menuName = "MovePattern/RotateAroundMove")]
public class RotateAroundMove : AttackPattern
{
    public float _radius = 1;
    Transform _bossTransform;
    Vector3 rotatePoint;
    GameObject rotateObj;
    public override void Initialize()
    {
        base.Initialize();
        _bossTransform = BossStatus.instance.Boss.transform;
        rotatePoint = _bossTransform.transform.position;
        rotateObj = new GameObject("BossRotatePoint");
        rotateObj.transform.SetParent(CameraController.instance.transform);
        rotateObj.transform.position = _bossTransform.position + new Vector3(0, _radius, 0);
    }
    public override void Start()
    {
    }
    public override void Update()
    {
        //Transform b = GameObject.Find("Boss").transform;
        //b.transform.position =
        //      new Vector3
        //      (
        //        rotatePoint.x + 2 * Mathf.Cos(Time.time),
        //        rotatePoint.y + 2 * Mathf.Sin(Time.time),
        //        0.0f
        //      );

        _bossTransform.RotateAround(rotateObj.transform.position, Vector3.forward, 120 * Time.deltaTime);
        _bossTransform.transform.rotation = Quaternion.identity;
    }
    public override void End()
    {
    }
}
