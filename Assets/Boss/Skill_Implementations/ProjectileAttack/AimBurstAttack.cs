﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AimBurstAttack", menuName = "AttackPattern/AimBurstAttack")]
public class AimBurstAttack : AttackPattern
{
    BossStatus BStatus;
    public ProjectileStatus BulletStatus;
    public int amount = 1;

    public override void Initialize()
    {
        base.Initialize();
        BStatus = BossStatus.instance;
    }
    public override void Start()
    {
    }
    public override void Update()
    {
        if (Check())
        {
            Fire();
        }
    }
    public override void End()
    {
    }

    void Fire()
    {
        Vector2 dir = (PlayerController.instance.transform.position - BStatus.transform.position).normalized;

        for (int i = 0; i < amount; i++)
        {
            var projectile = NormalBullet.Pool.Use(BStatus.Gun.transform.position, BStatus.Gun.transform.rotation);
            var bullet = projectile.GetComponent<NormalBullet>();
            dir = dir + new Vector2(-dir.x, dir.y) * Random.Range(0f, 1f);
            bullet.Initialize(dir, BulletStatus);
        }
    }
}