﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TurnLightAttack", menuName = "AttackPattern/TurnLightAttack")]
public class TurnLightAttack : AttackPattern
{
    public GameObject[] Lights;
    public bool TurnBool = false;

    private int LightIndex = 0;
    public override void Initialize()
    {
        base.Initialize();
    }
    public override void Start()
    {
    }
    public override void Update()
    {
        if (Check())
        {
            Fire();
        }
    }
    public override void End()
    {
    }

    void Fire()
    {
        if(LightIndex < Lights.Length)
        {
            Lights[LightIndex].SetActive(TurnBool);
            LightIndex++;
        }
    }
}