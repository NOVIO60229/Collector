﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NormalBulletAttack", menuName = "AttackPattern/NormalBulletAttack")]
public class NormalBulletAttack : AttackPattern
{
    BossStatus BStatus;
    public ProjectileStatus BulletStatus;
    public override void Initialize()
    {
        base.Initialize();
        BStatus = BossStatus.instance;
    }
    public override void Start()
    {
    }
    public override void Update()
    {
        if (Check())
        {
            Fire();
        }
    }
    public override void End()
    {
    }

    void Fire()
    {
        var projectile = NormalBullet.Pool.Use(BStatus.Gun.transform.position, BStatus.Gun.transform.rotation);
        var bullet = projectile.GetComponent<NormalBullet>();

        float radius = BStatus._angleZ * Mathf.Deg2Rad;
        Vector2 dir = new Vector2(Mathf.Cos(radius), Mathf.Sin(radius));
        bullet.Initialize(dir, BulletStatus);
    }
}