﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BurstAttack", menuName = "AttackPattern/BurstAttack")]
public class BurstAttack : AttackPattern
{
    BossStatus BStatus;
    public ProjectileStatus BulletStatus;

    private float time = 0;
    public float rotateSpeed = 1;
    public int amount = 1;

    public override void Initialize()
    {
        base.Initialize();
        BStatus = BossStatus.instance;
    }
    public override void Start()
    {
        time = 0;
    }
    public override void Update()
    {
        time += Time.deltaTime;

        if (Check())
        {
            Fire();
        }
    }
    public override void End()
    {
    }

    void Fire()
    {

        Vector2 dir = new Vector2(Mathf.Sin(time * rotateSpeed), Mathf.Cos(time * rotateSpeed));

        for (int i = 0; i < amount; i++)
        {
            var projectile = NormalBullet.Pool.Use(BStatus.Gun.transform.position, BStatus.Gun.transform.rotation);
            var bullet = projectile.GetComponent<NormalBullet>();
            bullet.Initialize(dir, BulletStatus);
        }
    }
}