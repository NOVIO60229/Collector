﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BulletWallAttack", menuName = "AttackPattern/BulletWallAttack")]
public class BulletWallAttack : AttackPattern
{
    public ProjectileStatus BulletStatus;
    public int BulletPerFire;
    public Vector2 Dir;

    CameraController Cam;
    ObjectPoolNew Pool;


    public override void Initialize()
    {
        base.Initialize();
        Cam = CameraController.instance;
        Pool = GravitylBullet.Pool;
    }
    public override void Start()
    {
    }
    public override void Update()
    {
        if (Check())
        {
            Fire();
        }
    }
    public override void End()
    {
    }

    void Fire()
    {
        for (int i = 0; i < BulletPerFire; i++)
        {
            var bullet = Pool.Use(new Vector3(Cam.transform.position.x - Cam._camera.m_Lens.OrthographicSize * Cam._camera.m_Lens.Aspect * 0.98f, Cam.transform.position.y + Random.Range(-Cam._camera.m_Lens.OrthographicSize, Cam._camera.m_Lens.OrthographicSize), 0), Quaternion.identity).GetComponent<GravitylBullet>();
            bullet.Initialize(Dir, BulletStatus);
        }

    }
}