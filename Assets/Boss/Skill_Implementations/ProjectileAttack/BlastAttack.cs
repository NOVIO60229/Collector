﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BlastAttack", menuName = "AttackPattern/BlastAttack")]
public class BlastAttack : AttackPattern
{
    [Header("Parent")]
    public ProjectileStatus ParentStatus;
    public Vector2 ParentDir = Vector2.zero;
    public float FlyBeatFull = 1f;
    public float WaitBeatFull = 1f;
    [Header("Child")]
    public ProjectileStatus ChildStatus;
    public int ChildAmount = 3;
    public override void Initialize()
    {
        base.Initialize();
    }
    public override void Start()
    {
    }
    public override void Update()
    {
        if (Check())
        {
            Fire();
        }
    }
    public override void End()
    {
    }

    void Fire()
    {
        //bad! because this is not mono, so I called another mono to do coroutine
        CameraController.instance.StartCoroutine(DoGenerateBullet());
    }
    IEnumerator DoGenerateBullet()
    {
        ObjectPoolNew pool = NoRecycleBullet.Pool;
        CameraController cam = CameraController.instance;

        //generate parent bullet in upper half of screen
        var parentBullet = pool.Use(cam.transform.position + new Vector3(
            Random.Range(-cam._camera.m_Lens.OrthographicSize * cam._camera.m_Lens.Aspect, cam._camera.m_Lens.OrthographicSize * cam._camera.m_Lens.Aspect) * 0.9f,
            cam._camera.m_Lens.OrthographicSize,
            0), Quaternion.identity).GetComponent<NoRecycleBullet>();

        parentBullet.Initialize(ParentDir, ParentStatus);
        yield return new WaitForSeconds(FlyBeatFull * BPM_Counter.instance._beatIntervalSec);
        parentBullet.SetDir(Vector2.zero);
        yield return new WaitForSeconds(WaitBeatFull * BPM_Counter.instance._beatIntervalSec);



        pool = NormalBullet.Pool;
        int thetaDeg = Random.Range(0, 360);
        var _angleInterval = 360 / ChildAmount;
        for (int i = 0; i < ChildAmount; i++)
        {
            var projectile = pool.Use(parentBullet.transform.position, parentBullet.transform.rotation);
            float radius = thetaDeg * Mathf.Deg2Rad;
            Vector2 dir = new Vector2(Mathf.Cos(radius), Mathf.Sin(radius));

            projectile.GetComponent<Projectile>().Initialize(dir, ChildStatus);

            thetaDeg += _angleInterval;
        }
        CameraController.instance.Shake();
        parentBullet.Recycle();

    }
}