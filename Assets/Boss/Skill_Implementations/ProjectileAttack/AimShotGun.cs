﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AimShotGun", menuName = "AttackPattern/AimShotGun")]
public class AimShotGun : AttackPattern
{
    public ProjectileStatus BulletStatus;
    public float _angleInterval = 10f;
    public int _targetAmount = 3;
    BossStatus BStatus;
    public override void Initialize()
    {
        base.Initialize();
        BStatus = BossStatus.instance;
    }
    public override void Start()
    {
    }
    public override void Update()
    {
        if (Check())
        {
            Fire();
            CameraController.instance.Shake();
        }
    }
    public override void End()
    {
    }

    void Fire()
    {
        float thetaDeg = BStatus._angleZ - (0.5f * _angleInterval * (_targetAmount - 1));

        BossStatus boss = BossStatus.instance;
        ObjectPoolNew pool = NormalBullet.Pool;

        for (int i = 0; i < _targetAmount; i++)
        {
            var projectile = pool.Use(boss.Gun.transform.position, boss.Gun.transform.rotation);
            float radius = thetaDeg * Mathf.Deg2Rad;
            Vector2 dir = new Vector2(Mathf.Cos(radius), Mathf.Sin(radius));

            projectile.GetComponent<Projectile>().Initialize(dir, BulletStatus);

            thetaDeg += _angleInterval;
        }
    }
}