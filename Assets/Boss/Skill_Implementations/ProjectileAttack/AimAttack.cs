﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AimAttack", menuName = "AttackPattern/AimAttack")]
public class AimAttack : AttackPattern
{
    BossStatus BStatus;
    public ProjectileStatus BulletStatus;
    public override void Initialize()
    {
        base.Initialize();
        BStatus = BossStatus.instance;
    }
    public override void Start()
    {
    }
    public override void Update()
    {
        if (Check())
        {
            Fire();
        }
    }
    public override void End()
    {
    }

    void Fire()
    {
        var projectile = NormalBullet.Pool.Use(BStatus.Gun.transform.position, BStatus.Gun.transform.rotation);
        var bullet = projectile.GetComponent<NormalBullet>();

        Vector2 dir = PlayerController.instance.transform.position - BStatus.transform.position;
        bullet.Initialize(dir, BulletStatus);
    }
}