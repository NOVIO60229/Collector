﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[CreateAssetMenu(fileName = "ProjectileAroundAttack", menuName = "AttackPattern/ProjectileAroundAttack")]
public class ProjectileAroundAttack : AttackPattern
{
    public ProjectileStatus BulletStatus;
    public int projectileAmount = 8;
    public float Radius = 1;
    public float RateStart = 1;
    public float RateEnd = 1;
    public float DeltaBeatFulls = 0f;
    //public AnimationCurve RateCurve;

    private bool IsRotating = false;
    private float Rate = 1;
    Transform BossTransform;
    GameObject[] Projectiles;
    public override void Initialize()
    {
        base.Initialize();
        BossTransform = BossStatus.instance.Boss.transform;
    }
    public override void Start()
    {
        InitValue();
        Fire();
        ani.PlayCharge();
    }
    public override void Update()
    {
        if (Check())
        {

        }

        if (IsRotating)
        {
            float baseAngle = Rate * Time.time;
            for (int i = 0; i < projectileAmount; ++i)
            {
                if (Projectiles[i] == null) continue;

                float angleOffset = 2.0f * Mathf.PI * i / projectileAmount;
                Projectiles[i].transform.position =
                  new Vector3
                  (
                    BossTransform.position.x + Radius * Mathf.Cos(baseAngle + angleOffset),
                    BossTransform.position.y + Radius * Mathf.Sin(baseAngle + angleOffset),
                    0.0f
                  );
            }
        }
    }

    public void Fire()
    {
        if (IsRotating) return;

        Projectiles = new GameObject[projectileAmount];
        for (int i = 0; i < projectileAmount; ++i)
        {
            Projectiles[i] = NoRecycleBullet.Pool.Use();
            Projectiles[i].GetComponent<Projectile>().Initialize(Vector2.zero, BulletStatus);
        }

        Rate = RateStart;
        float DeltaSec = DeltaBeatFulls * BPM_Counter.instance._beatIntervalSec;

        if (DeltaSec != 0)
        {
            DOTween.To(() => Rate, x => Rate = x, RateEnd, DeltaSec).SetEase(Ease.InQuad);
        }

        IsRotating = true;
    }

    public override void End()
    {
        foreach (GameObject g in Projectiles)
        {
            g.GetComponent<NoRecycleBullet>().Recycle();
        }
        ani.EndCharge();
    }

    private void InitValue()
    {
        IsRotating = false;
        Rate = 1;
    }
}