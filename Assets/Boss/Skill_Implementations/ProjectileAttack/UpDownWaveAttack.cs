﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[CreateAssetMenu(fileName = "UpDownWaveAttack", menuName = "AttackPattern/UpDownWaveAttack")]
public class UpDownWaveAttack : AttackPattern
{
    public ProjectileStatus BulletStatus;
    public int _targetAmount = 3;
    public float _upDownRange = 1;
    BossStatus BStatus;
    private int _dirMulti = 1;
    private BossRoom Room;

    public float startDegree;
    public float endDegree;
    public float turnBeat = 1;
    private float currentDegree = 0;
    public override void Initialize()
    {
        Room = BossRoom.Instance;
        base.Initialize();
        BStatus = BossStatus.instance;
    }
    public override void Start()
    {
        currentDegree = startDegree;
        DOTween.To(() => currentDegree, x => currentDegree = x, endDegree, bpm._beatIntervalSec * turnBeat);
    }
    public override void Update()
    {
        if (Check())
        {
            Fire();
            CameraController.instance.Shake();
        }
    }
    public override void End()
    {
    }

    private void Fire()
    {
        for (int i = 0; i < _targetAmount; i++)
        {
            var projectile = UpDownBullet.Pool.Use(BStatus.Gun.transform.position, BStatus.Gun.transform.rotation);
            var bullet = projectile.GetComponent<UpDownBullet>();

            float radius = currentDegree * Mathf.Deg2Rad;
            Vector2 dir = new Vector2(Mathf.Cos(radius), Mathf.Sin(radius));
            float verticalradius = (currentDegree + 90) * Mathf.Deg2Rad;
            Vector2 verticalDir = new Vector2(Mathf.Cos(verticalradius), Mathf.Sin(verticalradius));

            projectile.transform.position += (Vector3)(dir + _upDownRange * _dirMulti * verticalDir);
            if (_dirMulti > 0)
            {
                bullet._time = Mathf.PI;
            }
            _dirMulti *= -1;

            bullet.Initialize(dir, verticalDir, BulletStatus);
        }
    }
}

