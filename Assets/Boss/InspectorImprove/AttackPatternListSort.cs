﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class AttackPatternListSort : MonoBehaviour
{
    private int _listIndex = 1;
    public Boss _boss;
    private void Awake()
    {
        for (int i = 0; i < _boss._patternsLists.Length; i++)
        {
            _boss._patternsLists[i].name = _listIndex.ToString();
            _listIndex++;
        }
    }
}
