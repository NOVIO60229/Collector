﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Trap/DmgDealerStatus")]
public class DmgDealerStatus : ScriptableObject
{
    public int damage = 0;
}