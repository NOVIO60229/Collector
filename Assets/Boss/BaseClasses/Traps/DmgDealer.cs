﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DmgDealer : MonoBehaviour
{
    public int dmg;
    public bool IsActive;

    public void Switch(bool isActive)
    {
        IsActive = isActive;
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (IsActive)
        {
            IDamagable target = other.GetComponent<IDamagable>();
            target?.Hurt(dmg, transform);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (IsActive)
        {
            IDamagable target = collision.collider.GetComponent<IDamagable>();
            target?.Hurt(dmg, transform);
        }
    }
}