﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ProjectileStatus")]
public class ProjectileStatus : ScriptableObject
{
    public Sprite sprite;
    public int damage = 10;
    public bool isAutoMove = true;
    [Header("Speed Change")]
    public float speedInit = 10;
    public float speedEnd = 10;
    public float accelerateBeat = 0;
    public AnimationCurve speedCurve;
    [Header("Scale Change")]
    public float scaleInit = 1;
    public float scaleEnd = 1;
    public float scaleChangeBeat = 0;
    public AnimationCurve scaleCurve;
}
