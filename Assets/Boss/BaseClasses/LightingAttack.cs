﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightingAttack : MonoBehaviour
{
    public int _damage = 0;
    protected float PersistSec = 0;
    protected float StartTime = 0;

    private void Awake()
    {
    }
    private void OnEnable()
    {
        ResetValue();
    }
    private void OnDisable()
    {
    }
    public virtual void Initialize(Vector3 position, float degree, float persistSec)
    {
        transform.position = position;
        transform.rotation = Quaternion.Euler(0, 0, degree);
        PersistSec = persistSec;
    }

    private void Update()
    {
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
    }
    public virtual void Recycle()
    {
        print("Not assign any pool to recycle");
    }
    protected virtual void ResetValue()
    {
        StartTime = Time.time;
    }
}