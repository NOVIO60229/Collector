﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class Boss : MonoBehaviour
{
    //public AttackPattern[] _randomPatterns;
    public AttackPatternList[] _patternsLists = new AttackPatternList[61];

    private int _onBeatIndex = 0;

    private void OnEnable()
    {
        BPM_Counter.OnBeatFullevery4 += TriggerExcute;
    }
    private void OnDisable()
    {
        BPM_Counter.OnBeatFullevery4 -= TriggerExcute;
    }


    //為了讓協程能註冊BPM_Counter中的事件
    public void TriggerExcute()
    {
        if (_onBeatIndex < _patternsLists.Length)
        {
            for (int i = 0; i < _patternsLists[_onBeatIndex].patterns.Length; i++)
            {
                StartCoroutine(Excute(_onBeatIndex, i));
            }
            _onBeatIndex++;
        }
    }
    IEnumerator Excute(int onBeatIndex, int forLoopIndex)
    {
        int beatFulls = 0; int beatD8s = 0;
        var pattern = _patternsLists[onBeatIndex].patterns[forLoopIndex];
        if (pattern == null) yield break;

        pattern.Initialize();
        pattern.Start();
        while (beatFulls < pattern.beatFull)
        {
            if (BPM_Counter.instance._isBeatFull)
            {
                beatFulls++;
            }
            pattern.Update();
            yield return null;
        }
        while (beatD8s < pattern.beatD8)
        {
            if (BPM_Counter.instance._isBeatD8)
            {
                beatD8s++;
            }
            pattern.Update();
            yield return null;
        }
        pattern.End();
    }

    //private void RandomAttack()
    //{
    //    int r = Random.Range(0, _randomPatterns.Length);
    //    if (_randomPatterns[r] != null)
    //    {
    //        _randomPatterns[r].Initialize(transform.position, Vector3.zero);
    //    }
    //    else
    //    {
    //        Debug.LogWarning("attack pattern is null");
    //    }
    //}
}