﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticleLighting : LightingAttack
{
    private static ObjectPoolNew _pool;
    public static ObjectPoolNew Pool
    {
        get
        {
            if (_pool == null)
            {
                GameObject g = new GameObject("VerticleLightingPool");
                var pool = g.AddComponent<ObjectPoolNew>();
                pool.InitPool(PrefabHandler.Instance.VerticleLighting, 4);
                _pool = pool;
            }
            return _pool;
        }
    }

    public ParticleSystem particle;
    private void Start()
    {
        StartTime = Time.time;
    }
    private void Update()
    {
        if (Time.time - StartTime > PersistSec)
        {
            Recycle();
        }
    }
    public override void Recycle()
    {
        Pool.Recycle(gameObject);
    }
}
