﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossRoom : MonoBehaviour
{
    public static BossRoom Instance;
    private void Awake()
    {
        Instance = this;
    }

    public GameObject RoomTopLeftPoint;
    public GameObject RoomBottomRightPoint;
    public GameObject PunchPoint1;
    public GroundHook GroundHook;
    public GameObject[] Rocks;
    private bool IsRockDown = false;

    [HideInInspector]
    public float VerticalDistance;
    [HideInInspector]
    public float HorizontalDistance;

    private void Start()
    {
        foreach (GameObject rock in Rocks)
        {
            rock.SetActive(false);
        }

        MeasureRoom();
    }
    void MeasureRoom()
    {
        VerticalDistance = RoomTopLeftPoint.transform.position.y - RoomBottomRightPoint.transform.position.y;
        HorizontalDistance = RoomBottomRightPoint.transform.position.x - RoomTopLeftPoint.transform.position.x;
    }

    //dviision將空間切成X塊，point從最左邊為0，最右邊為division的數值
    public Vector3 RoomPositionDivided(int divisionX, float pointX, int divisionY, float pointY)
    {
        float h = 0;
        float y = 0;
        if (divisionX != 0)
        {
            h = HorizontalDistance / divisionX;
        }
        if (divisionY != 0)
        {
            y = VerticalDistance / divisionY;
        }

        Vector3 point = RoomTopLeftPoint.transform.position;
        point += new Vector3(h * (pointX), -y * (pointY), 0);

        return point;
    }

    public void RocksDown()
    {
        if (!IsRockDown)
        {
            StartCoroutine(DoRocksDown());
            IsRockDown = true;
        }
    }
    IEnumerator DoRocksDown()
    {
        Rocks[0].SetActive(true);

        yield return new WaitForSeconds(0.5f);

        BossAnimation.Instance.Animator.Play("Shock");
        Rocks[1].SetActive(true);
        Rocks[2].SetActive(true);

        yield return new WaitForSeconds(0.5f);

        BossAnimation.Instance.GetComponent<SpriteRenderer>().DOColor(Color.gray, 1f);
        BossAnimation.Instance.Animator.Play("Hit");
    }
}
