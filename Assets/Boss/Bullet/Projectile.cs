﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using CjLib;

[RequireComponent(typeof(SpriteRenderer))]
public class Projectile : MonoBehaviour
{
    public ProjectileStatus _status;
    private SpriteRenderer _spriteRenderer;
    protected int _damage = 0;
    protected float _speed = 0;
    protected bool _isAutoMove = true;
    protected Vector3 _dir = Vector2.zero;

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _spriteRenderer.sprite = _status.sprite;

        var collider = gameObject.AddComponent<PolygonCollider2D>();
        collider.isTrigger = true;
    }
    private void OnEnable()
    {
        ResetValue();
    }
    private void OnDisable()
    {
    }
    public virtual void Initialize(Vector3 dir, ProjectileStatus status)
    {
        _status = status;
        _dir = dir.normalized;
        _speed = _status.speedInit;

        if (_status.accelerateBeat != 0)
        {
            DOTween.To(() => _speed, x => _speed = x, _status.speedEnd, _status.accelerateBeat * BPM_Counter.instance._beatIntervalSec).SetEase(_status.speedCurve);
        }

        transform.localScale = new Vector3(_status.scaleInit, _status.scaleInit, 1);

        if (_status.scaleChangeBeat != 0)
        {
            transform.DOScale(_status.scaleEnd, _status.scaleChangeBeat * BPM_Counter.instance._beatIntervalSec).SetEase(_status.scaleCurve);
        }

        _damage = _status.damage;
        _isAutoMove = _status.isAutoMove;
    }
    public void SetDir(Vector3 dir)
    {
        _dir = dir.normalized;
    }

    protected virtual void Update()
    {
        if (_isAutoMove)
        {
            Move();
        }

    }
    protected virtual void Move()
    {
        transform.position += _dir * _speed * Time.deltaTime;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        IDamagable damagable = collision.GetComponent<IDamagable>();
        if (damagable != null)
        {
            damagable.Hurt(_damage, transform);
        }
        Recycle();
    }
    public virtual void Recycle()
    {
        print("Not assign any pool to recycle");
    }
    protected virtual void ResetValue()
    {
    }

    void OnDrawGizmos()
    {

        GizmosUtil.DrawLine(transform.position, transform.position + _dir * 20f, Color.red);
    }
}