﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpDownBullet : Projectile
{
    public float UpDownRate = 1f;
    public float UpDownDistanceMultiplier = 1f;
    private Vector3 _verticalDir = Vector2.zero;
    [HideInInspector] public float _time = 0;

    private static ObjectPoolNew _pool;
    public static ObjectPoolNew Pool
    {
        get
        {
            if (_pool == null)
            {
                GameObject g = new GameObject("UpDownBulletPool");
                var pool = g.AddComponent<ObjectPoolNew>();
                pool.InitPool(PrefabHandler.Instance.UpDownBullet, 20);
                _pool = pool;
            }
            return _pool;
        }
    }

    public void Initialize(Vector2 dir, Vector2 verticalDir, ProjectileStatus status)
    {
        base.Initialize(dir, status);
        _verticalDir = verticalDir;
    }
    protected override void Move()
    {
        transform.position += _verticalDir * Mathf.Cos(_time * UpDownRate) * UpDownDistanceMultiplier * Time.deltaTime;
        _time += Time.deltaTime;
        base.Move();
    }
    public override void Recycle()
    {
        _pool.Recycle(gameObject);
    }

    protected override void ResetValue()
    {
        _time = 0;
    }
}
