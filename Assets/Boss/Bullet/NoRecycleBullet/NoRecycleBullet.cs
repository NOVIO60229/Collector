﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoRecycleBullet : Projectile
{
    private static ObjectPoolNew _pool;
    public static ObjectPoolNew Pool
    {
        get
        {
            if (_pool == null)
            {
                GameObject g = new GameObject("NoRecycleBulletPool");
                var pool = g.AddComponent<ObjectPoolNew>();
                pool.InitPool(PrefabHandler.Instance.NoRecycleBullet, 6);
                _pool = pool;
            }
            return _pool;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        IDamagable damagable = collision.GetComponent<IDamagable>();
        if (damagable != null)
        {
            damagable.Hurt(_damage, transform);
        }
    }
    public override void Recycle()
    {
        _pool.Recycle(gameObject);
    }
}
