﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravitylBullet : Projectile
{
    [HideInInspector] public float _time = 0;
    public float GravityMultiplier = 1f;

    private static ObjectPoolNew _pool;
    public static ObjectPoolNew Pool
    {
        get
        {
            if (_pool == null)
            {
                GameObject g = new GameObject("GravitylBullet");
                var pool = g.AddComponent<ObjectPoolNew>();
                pool.InitPool(PrefabHandler.Instance.GravitylBullet, 10);
                _pool = pool;
            }
            return _pool;
        }
    }
    protected override void Move()
    {
        _time += Time.deltaTime;
        transform.position += new Vector3(0, Physics2D.gravity.y * GravityMultiplier * _time, 0) * Time.deltaTime;
        base.Move();
    }

    public override void Recycle()
    {
        _pool.Recycle(gameObject);
    }
    protected override void ResetValue()
    {
        _time = 0;
    }

}
