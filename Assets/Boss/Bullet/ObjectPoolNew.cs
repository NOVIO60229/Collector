﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolNew : MonoBehaviour
{
    private Queue<GameObject> Pool = new Queue<GameObject>();
    private GameObject Prefab;

    public void InitPool(GameObject prefab, int initAmount)
    {
        Prefab = prefab;
        for (int i = 0; i < initAmount; i++)
        {
            GameObject newPrefab = Instantiate(Prefab, transform);
            Pool.Enqueue(newPrefab);
            newPrefab.SetActive(false);
        }
    }

    public GameObject Use()
    {
        if (Pool.Count > 0)
        {
            GameObject prefab = Pool.Dequeue();
            prefab.SetActive(true);
            return prefab;
        }
        else
        {
            GameObject newPrefab = Instantiate(Prefab, transform);
            newPrefab.SetActive(false);
            newPrefab.SetActive(true);
            return newPrefab;
        }
    }
    public GameObject Use(Vector3 position, Quaternion rotation)
    {
        if (Pool.Count > 0)
        {
            GameObject prefab = Pool.Dequeue();
            prefab.transform.position = position;
            prefab.transform.rotation = rotation;
            prefab.SetActive(true);
            return prefab;
        }
        else
        {
            GameObject newPrefab = Instantiate(Prefab, position, rotation, transform);
            newPrefab.SetActive(false);
            newPrefab.SetActive(true);
            return newPrefab;
        }
    }


    public void Recycle(GameObject prefab)
    {
        prefab.gameObject.SetActive(false);
        Pool.Enqueue(prefab);
    }
}