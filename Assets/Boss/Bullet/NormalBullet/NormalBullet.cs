﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalBullet : Projectile
{
    private static ObjectPoolNew _pool;
    public static ObjectPoolNew Pool
    {
        get
        {
            if (_pool == null)
            {
                GameObject g = new GameObject("NormalBulletPool");
                var pool = g.AddComponent<ObjectPoolNew>();
                pool.InitPool(PrefabHandler.Instance.NormalBullet, 10);
                _pool = pool;
            }
            return _pool;
        }
    }

    private float startTime;
    public float DieAfterSec;

    private void Start()
    {
        startTime = Time.time;
    }
    protected override void Update()
    {
        base.Update();
        CheckDie();
    }
    void CheckDie()
    {
        if (Time.time - startTime > DieAfterSec)
        {
            Recycle();
        }
    }
    protected override void ResetValue()
    {
        startTime = Time.time;
    }
    public override void Recycle()
    {
        _pool.Recycle(gameObject);
    }
}
