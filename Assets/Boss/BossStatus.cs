﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStatus : MonoBehaviour
{
    public static BossStatus instance;

    public Boss Boss { get; private set; }
    public GameObject Gun;
    public GameObject SplashParticle;
    public GameObject ShieldBlastParticle1;
    public GameObject ShieldBlastParticle2;
    public GameObject ShieldBlastParticle3;
    public GameObject CircleParticle;

    public GameObject target;
    public bool IsFollowingTarget;
    public float followSpeed = 1;

    private Animator ani;

    [HideInInspector] public float _angleZ = 0;

    private void Awake()
    {
        ani = GetComponent<Animator>();
        instance = this;
        Boss = GetComponent<Boss>();
    }
    private void Update()
    {
        UpdateStatus();
        FollowTarget();
        Flip();
    }
    private void Flip()
    {
        PlayerController p = PlayerController.instance;
        if (p != null)
        {
            if (transform.position.x > p.transform.position.x)
            {
                transform.rotation = Quaternion.Euler(0f, 180f, 0f);
            }
            else
            {
                transform.rotation = Quaternion.Euler(0f, 0f, 0f);
            }
        }
    }



    private void UpdateStatus()
    {
        if (PlayerController.instance != null)
        {
            Gun.transform.LookAt(PlayerController.instance.transform.position, Vector3.right);
        }

        _angleZ = Gun.transform.eulerAngles.z;
    }
    private void FollowTarget()
    {
        if (!IsFollowingTarget) return;

        Vector3 movePoint = Vector3.Lerp(transform.position, target.transform.position, followSpeed * Time.deltaTime);
        transform.position = movePoint;
    }
    public void SetIsFollow(bool b)
    {
        IsFollowingTarget = b;
    }

    public void PlaySplash(float sec)
    {
        StartCoroutine(DoSplash(sec));
    }
    public void PlayShieldBlast(float sec)
    {
        StartCoroutine(DoShieldBlast(sec));
    }
    public void PlayCircle(float sec)
    {
        StartCoroutine(DoCircle(sec));
    }
    private IEnumerator DoSplash(float sec)
    {
        ani.Play("Shield");
        SplashParticle.SetActive(true);
        yield return new WaitForSeconds(sec);
        SplashParticle.SetActive(false);

        ani.Play("ShieldBlast");
        PlayShieldBlast(0.2f);
    }
    private IEnumerator DoShieldBlast(float sec)
    {
        ShieldBlastParticle1.SetActive(true);
        yield return new WaitForSeconds(sec);
        ShieldBlastParticle1.SetActive(false);


        ShieldBlastParticle2.SetActive(true);
        yield return new WaitForSeconds(sec);
        ShieldBlastParticle2.SetActive(false);


        ShieldBlastParticle3.SetActive(true);
        yield return new WaitForSeconds(sec);
        ShieldBlastParticle3.SetActive(false);
    }
    private IEnumerator DoCircle(float sec)
    {
        CircleParticle.SetActive(true);
        yield return new WaitForSeconds(sec);
        CircleParticle.SetActive(false);
    }
    public void PlayFly(float sec)
    {
        StartCoroutine(DoFly(sec));
    }
    private IEnumerator DoFly(float sec)
    {
        ani.Play("Move");
        yield return new WaitForSeconds(sec);
        ani.Play("Idle");
    }
}
