﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScorePanel : MonoBehaviour
{
    public Text Time;
    public Text Reward;
    public Text Rank;

    private static ScorePanel _instance;
    public static ScorePanel Instance
    {
        get
        {
            if (_instance == null)
            {
                var i = Instantiate(PrefabHandler.Instance.ScorePanel).GetComponent<ScorePanel>();
                _instance = i;
                DontDestroyOnLoad(i.gameObject);
            }
            return _instance;
        }
    }
    public void ShowScorePanel(float time)
    {
        gameObject.SetActive(true);
        SetScore(time);
    }
    public void HideScorePanel()
    {
        gameObject.SetActive(false);
        Clear();
    }
    public void SetScore(float time)
    {
        Time.text = ConvertSecToTime(time);
        GetReward();
        GetRank();
    }
    private string ConvertSecToTime(float sec)
    {
        float s = Mathf.Floor(sec % 60);
        string seconds = s < 10 ? "0" + s.ToString() : s.ToString();
        sec /= 60;
        float m = Mathf.Floor(sec % 60);
        string minutes = m < 10 ? "0" + m.ToString() : m.ToString();
        string time = minutes + ":" + seconds;

        return time;
    }
    private void GetReward()
    {

    }
    private void GetRank()
    {

    }
    private void Clear()
    {
        Time.text = "";
        //Reward.text = "";
        //Rank.text = "";
    }
    public void GoBackLevelSelect()
    {
        SceneCutter.Instance.LoadScene("LevelSelect", 2f);
        HideScorePanel();
    }
}
