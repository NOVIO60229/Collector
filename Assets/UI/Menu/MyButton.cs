﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class MyButton : MonoBehaviour, ISelectHandler, IPointerEnterHandler
{
    public string SelectedSoundName;

    public void OnPointerEnter(PointerEventData eventData)
    {
        CallButtonClickSFX();
    }
    public void OnSelect(BaseEventData eventData)
    {
        CallButtonClickSFX();
    }
    public void CallButtonClickSFX()
    {
        SoundHandler.Instance.PlaySFX(SelectedSoundName);
    }
}
