﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;

public class MenuFuncs : MonoBehaviour
{
    public void OnStart(Button btn)
    {
        btn.interactable = false;
        StartCoroutine(DOStartEffect());
        //load next scene
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    IEnumerator DOStartEffect()
    {
        yield return new WaitForSeconds(1);
        SceneCutter.Instance.LoadScene("LevelSelect", 2f);
    }
    public void BreathingLamp(Light2D light)
    {
        light.intensity = Mathf.Abs(Mathf.Sin(Time.time / 2));
    }


    public void OnExit()
    {
        Application.Quit();
    }
}
