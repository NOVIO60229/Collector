﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Level X Data", menuName = "CommissionData")]
public class CommissionData : ScriptableObject
{
    public string Name;
    public string LevelDescription;
    public Sprite GoodsOutLine;
    public Sprite BossOutLine;
    public string Reward;

    public string LevelToLoad;
}
