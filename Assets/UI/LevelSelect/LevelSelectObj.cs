﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectObj : MonoBehaviour
{
    public CommissionData Data;
    public void OnSelected()
    {
        LevelSelectManager.Instance.Commission.Pop(Data);
    }
    public void OnDeSelected()
    {
    }
    public void OnClicked()
    {
        LevelSelectManager.Instance.Commission.Fixed();
    }
}
