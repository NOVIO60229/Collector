﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class LevelSelectManager : MonoBehaviour
{
    public static LevelSelectManager Instance;

    public EventSystem EventSystem;
    public Commission Commission;
    public Button InitialBtn;

    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
    }
    private void Update()
    {
        if (EventSystem.currentSelectedGameObject == null)
        {
            Commission.Hide();
        }
    }
    public void OnSubmit(InputValue value)
    {
        if (EventSystem.currentSelectedGameObject == null)
        {
            InitialBtn.Select();
        }
    }
    public void OnCancel(InputValue value)
    {
        if (EventSystem.currentSelectedGameObject == null)
        {
            BackToMenu();
        }
        else
        {
            EventSystem.SetSelectedGameObject(null);
        }
    }

    public void BackToMenu()
    {
        SceneManager.LoadSceneAsync("Menu");
    }
}
