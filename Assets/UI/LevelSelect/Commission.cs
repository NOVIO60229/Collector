﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class Commission : MonoBehaviour
{
    enum State
    {
        Hide,
        Show,
        Fixed
    }
    public CommissionData Data;

    [Header("Child GameObjects")]
    public Text Name;
    public Text LevelDescription;
    public Image GoodsOutLine;
    public Image BossOutLine;
    public Text Reward;
    public Image Image;


    [Header("Animation Variables")]
    public float PopSec = 0.3f;

    private Vector3 OriginPosition, OriginEuler, OriginScale;
    private State _State = State.Hide;


    private void Awake()
    {
        SetOrigins();
    }
    private void SetOrigins()
    {
        OriginPosition = transform.position;
        OriginEuler = transform.eulerAngles;
        OriginScale = transform.localScale;
    }
    private void Update()
    {
        UpdateData();

        if (_State == State.Fixed)
        {
            //發光特效
        }
        else
        {
            //特效關閉
        }
    }
    void UpdateData()
    {
        Name.text = Data.Name;
        LevelDescription.text = Data.LevelDescription;
        GoodsOutLine.sprite = Data.GoodsOutLine;
        BossOutLine.sprite = Data.BossOutLine;
        Reward.text = Data.Reward;
    }

    public void Pop(CommissionData data)
    {
        _State = State.Show;
        Data = data;
        gameObject.SetActive(true);
        transform.DOScaleX(1, PopSec).From(0f, true);
    }

    public void Fixed()
    {
        StartCoroutine(EnterLevel());
        //if (_State != State.Fixed)
        //{
        //    _State = State.Fixed;
        //}
        //else
        //{
        //}
    }

    public void Hide()
    {
        if (_State != State.Hide)
        {
            _State = State.Hide;
            gameObject.SetActive(false);
        }
    }
    IEnumerator EnterLevel()
    {
        yield return new WaitForSeconds(1.5f);
        SceneCutter.Instance.LoadScene(Data.LevelToLoad, 1f);
    }
}
