﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using DG.Tweening;

public class CameraController : MonoSingleton<CameraController>
{
    public CinemachineBrain _brain;

    [HideInInspector] public CinemachineVirtualCamera _camera;
    CinemachineImpulseSource _impulse;
    Rigidbody2D _rb;
    public float _orthographicSizeOut = 17.55f;
    public float _orthographicSizeIn = 10;
    public float _orthographicSizeNormal = 15;
    public float _smoothTime = 1f;
    private float _orthographicSize = 0;
    private Tween _tween;

    private bool _isStartScrolling = false;
    public float _scrollSpeed = 5;

    private void Start()
    {
        if (_brain == null)
        {
            _brain = Camera.main.GetComponent<CinemachineBrain>();
        }
        _impulse = GetComponent<CinemachineImpulseSource>();
        _rb = GetComponent<Rigidbody2D>();
        UpdateCamera();
        _orthographicSize = _camera.m_Lens.OrthographicSize;

    }
    private void OnEnable()
    {
        BossLevelManager.OnLevelBossStartAttack += StartMove;
    }
    private void OnDisable()
    {
        BossLevelManager.OnLevelBossStartAttack -= StartMove;
    }
    //Note: 同一時間只能有一個Vcam是active
    private void UpdateCamera()
    {
        //GameObject vCamGameObject = _brain.ActiveVirtualCamera.VirtualCameraGameObject;
        _camera = FindObjectOfType<CinemachineVirtualCamera>();
        //CinemachineVirtualCamera camera = target.GetComponent<CinemachineVirtualCamera>();
        //CinemachineImpulseSource impulse = target.GetComponent<CinemachineImpulseSource>();
        if (_camera == null)
        {
            print("can't find caemra");
        }
        if (_impulse == null)
        {
            print("can't find impulse source");
        }
    }

    private void FixedUpdate()
    {
        if (_isStartScrolling)
        {
            _rb.velocity = Vector3.right * _scrollSpeed;
        }
        if (transform.position.x >= 204.39f)
        {
            StoptMove();
        }
    }

    public void ZoomIn()
    {
        StartCoroutine(TweenOrthographicSize(_orthographicSizeIn));
        _camera.m_Lens.OrthographicSize = _orthographicSize;
    }
    public void ZoomOut()
    {
        StartCoroutine(TweenOrthographicSize(_orthographicSizeOut));
        _camera.m_Lens.OrthographicSize = _orthographicSize;
    }
    public void ZoomNormal()
    {
        StartCoroutine(TweenOrthographicSize(_orthographicSizeNormal));
        _camera.m_Lens.OrthographicSize = _orthographicSize;
    }

    private IEnumerator TweenOrthographicSize(float endSize)
    {
        UpdateCamera();
        if (_camera.m_Lens.OrthographicSize == endSize) yield break;
        _tween.Kill();
        _tween = DOTween.To(() => _orthographicSize, x => _orthographicSize = x, endSize, _smoothTime);
        while (_tween.IsPlaying())
        {
            _camera.m_Lens.OrthographicSize = _orthographicSize;
            yield return null;
        }
    }


    public void Shake()
    {
        _impulse.GenerateImpulse();
    }

    public void StartMove()
    {
        _isStartScrolling = true;
    }
    public void StoptMove()
    {
        _isStartScrolling = false;
    }
}
