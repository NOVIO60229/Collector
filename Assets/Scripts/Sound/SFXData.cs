﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "Sound/SFX")]
public class SFXData : ScriptableObject
{
    public string Name;
    public AudioClip[] AudioClips = new AudioClip[1];
    public AudioMixerGroup Mixer;

    [MinMaxSlider]
    [SerializeField]
    private Vector2 MinMaxVolume;
    public float Volume
    {
        get
        {
            float volume = Random.Range(MinMaxVolume.x, MinMaxVolume.y);
            return volume;
        }
    }

    [MinMaxSlider(0, 2f)]
    [SerializeField]
    private Vector2 MinMaxPitch;
    public float Pitch
    {
        get
        {
            float pitch = Random.Range(MinMaxPitch.x, MinMaxPitch.y);
            return pitch;
        }
    }

    public AudioClip Clip
    {
        get
        {
            AudioClip audioclip = AudioClips[Random.Range(0, AudioClips.Length)];
            return audioclip;
        }
    }
}
