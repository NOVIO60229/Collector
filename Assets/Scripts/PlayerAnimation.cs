﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    private Animator Animator;
    private PlayerController Controller;
    [HideInInspector] public string StateName = "Idle";
    private float NormalizedTime = 0;
    private float PlayTimeMultiplier = 1;

    public Dictionary<string, float> _aniSecDic = new Dictionary<string, float>() {
        {"Idle",1f},{ "JumpUp",1f},{"JumpDown",2f},{"Slide" ,0.2f},{"Throw", 0.6f},{"JumpThrow",0.5f},{"Crouch",0.8f},{"CrouchIdle",1f},{"SlideToIdle", 0.1f},{"Dead", 0.8f},{"Hurt", 1f}
    };

    private void Start()
    {
        Animator = GetComponent<Animator>();
        Controller = GetComponent<PlayerController>();
    }
    private void Update()
    {
        string name = GetBackPackAniName();
        Animator.Play(name, 0, NormalizedTime);
        NormalizedTime += Time.deltaTime * PlayTimeMultiplier;
        Mathf.Repeat(NormalizedTime, 1);
    }
    public void Play_Run(float speed)
    {
        string _name = "Run";
        float playSec = Remap(speed, 0, 40, 0.4f, 1f);
        ChangeStateNameAndPlayrate(_name, playSec, false);
    }

    public void PlayAnimationOnce(string aniName, float playSecond)
    {
        ChangeStateNameAndPlayrate(aniName, playSecond);
    }
    public void PlayAnimationOnce(string aniName)
    {
        ChangeStateNameAndPlayrate(aniName, _aniSecDic[aniName]);
    }
    public void PlayAnimation(string aniName, float playSecond)
    {
        if (aniName == StateName) return;
        ChangeStateNameAndPlayrate(aniName, playSecond);
    }
    public void PlayAnimation(string aniName)
    {
        if (aniName == StateName) return;
        ChangeStateNameAndPlayrate(aniName, _aniSecDic[aniName]);
    }
    //public void Play_Animation(string transition, string aniName, float transitionPlaySecond, float playSecond)
    //{
    //    if (transition == StateName || aniName == StateName) return;
    //    StartCoroutine(StartTransition(transition, aniName, transitionPlaySecond, playSecond));
    //}
    //public void Play_Animation(string transition, string aniName)
    //{
    //    if (transition == StateName || aniName == StateName) return;
    //    StartCoroutine(StartTransition(transition, aniName, _aniSecDic[transition], _aniSecDic[aniName]));
    //}


    public void ChangeStateNameAndPlayrate(string name, float _playTime, bool playFromStart = true)
    {
        StateName = name;
        PlayTimeMultiplier = 1 / _playTime;

        if (playFromStart)
        {
            NormalizedTime = 0;
        }
    }

    public string GetBackPackAniName()
    {
        string name = StateName;

        if (name == "Throw" || name == "JumpThrow")
        {
            return name;
        }

        if (Controller.IsBackpackOnBack)
        {
            name += "B";
        }
        return name;
    }

    //IEnumerator StartTransition(string transition, string targetAnimation, float transitionPlaySecond, float playSecond)
    //{
    //    ChangeStateNameAndPlayrate(transition, transitionPlaySecond);
    //    yield return new WaitForSeconds(transitionPlaySecond);

    //    //make sure transition has not been replaced
    //    if (StateName == transition)
    //        ChangeStateNameAndPlayrate(targetAnimation, playSecond);

    //}
    private float Remap(float value, float from1, float to1, float from2, float to2)
    {
        return to2 - (value - from1) / (to1 - from1) * (to2 - from2);
    }

}
