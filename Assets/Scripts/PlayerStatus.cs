﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public enum State
{
    normal,
    slide,
    crouch,
    hurt
}
public class PlayerStatus : MonoBehaviour
{
    //run variables
    public const float BaseRunSpeed = 22f;
    List<RunSpeedEffect> RunSpeedEffectList = new List<RunSpeedEffect>();

    List<ForceEffect> ForceEffectList = new List<ForceEffect>();



    //slide variables
    public const float BaseSlideSpeed = 5f;

    //crouch variables
    public const float BaseCrouchSpeed = 6f;

    private int Stamina = 100;


    //Speed
    public float RunSpeed
    {
        get
        {
            return BaseRunSpeed + GetRunSpeedEffectSum();
        }
    }
    public float SlideSpeed
    {
        get
        {
            return BaseSlideSpeed + GetRunSpeedEffectSum();
        }
    }
    public float CrouchSpeed
    {
        get
        {
            return BaseCrouchSpeed + GetRunSpeedEffectSum();
        }
    }
    public Vector2 Force
    {
        get
        {
            return GetForceEffectSum();
        }
    }

    public void AddMoveSpeed(float speed, float sec, Action startAction = null, Action endAction = null)
    {
        RunSpeedEffect effect = new RunSpeedEffect(speed, sec, startAction, endAction);
        RunSpeedEffectList.Add(effect);
    }
    public void AddMoveSpeed(float speed, float sec, Ease ease, Action startAction = null, Action endAction = null)
    {
        RunSpeedEffect effect = new RunSpeedEffect(speed, sec, ease, startAction, endAction);
        RunSpeedEffectList.Add(effect);
    }
    public void AddForce(Vector2 dir, float speed, float sec, Action startAction = null, Action endAction = null)
    {
        ClearForce();
        ForceEffect effect = new ForceEffect(dir, speed, sec, startAction, endAction);
        ForceEffectList.Add(effect);
        SoundHandler.Instance.PlaySFX("SpeedUp");
    }
    public void ClearForce()
    {
        ForceEffectList.Clear();
    }

    float GetRunSpeedEffectSum()
    {
        float SpeedChange = 0;
        foreach (RunSpeedEffect effect in RunSpeedEffectList)
        {
            SpeedChange += effect.GetSpeed();
        }
        return SpeedChange;
    }
    Vector2 GetForceEffectSum()
    {
        Vector2 dir = Vector2.zero;
        foreach (ForceEffect effect in ForceEffectList)
        {
            dir += effect.GetSpeed();
        }
        return dir;
    }

    private void Update()
    {
        UpdateRunSpeedEffectList();
        UpdateForceEffectList();
    }
    void UpdateRunSpeedEffectList()
    {
        for (int i = RunSpeedEffectList.Count - 1; i >= 0; i--)
        {
            if (RunSpeedEffectList[i].IsEffectEnd())
            {
                RunSpeedEffectList[i].OnRemove();
                RunSpeedEffectList.RemoveAt(i);
            }
        }
    }
    void UpdateForceEffectList()
    {
        for (int i = RunSpeedEffectList.Count - 1; i >= 0; i--)
        {
            if (RunSpeedEffectList[i].IsEffectEnd())
            {
                RunSpeedEffectList[i].OnRemove();
                RunSpeedEffectList.RemoveAt(i);
            }
        }
    }

}
class RunSpeedEffect
{
    float Speed;
    float DurationSec;
    float StartTime;
    Action StartAction;
    Action EndAction;
    public RunSpeedEffect(float speed, float durationSec, Action startAction, Action endAction)
    {
        DurationSec = durationSec;
        StartTime = Time.time;
        StartAction = startAction;
        EndAction = endAction;
        DOTween.To(() => Speed, x => Speed = x, 0, durationSec).From(speed, true).SetEase(Ease.OutQuart);

        OnStart();
    }
    public RunSpeedEffect(float speed, float durationSec, Ease ease, Action startAction, Action endAction)
    {
        DurationSec = durationSec;
        StartTime = Time.time;
        StartAction = startAction;
        EndAction = endAction;
        DOTween.To(() => Speed, x => Speed = x, 0, durationSec).From(speed, true).SetEase(ease);

        OnStart();
    }

    public bool IsEffectEnd()
    {
        return Time.time - StartTime > DurationSec;
    }

    public float GetSpeed()
    {
        return Speed;
    }

    public void OnStart()
    {
        StartAction?.Invoke();
    }
    public void OnRemove()
    {
        EndAction?.Invoke();
    }
}
class ForceEffect
{
    Vector2 Dir;
    float Speed;
    float DurationSec;
    float StartTime;
    Action StartAction;
    Action EndAction;
    public ForceEffect(Vector2 dir, float speed, float durationSec, Action startAction, Action endAction)
    {
        Dir = dir;
        DurationSec = durationSec;
        StartTime = Time.time;
        StartAction = startAction;
        EndAction = endAction;
        DOTween.To(() => Speed, x => Speed = x, 0, DurationSec * 0.5f).From(speed, true).SetEase(Ease.OutQuad).SetDelay(DurationSec * 0.5f);

        OnStart();
    }

    public bool IsEffectEnd()
    {
        return Time.time - StartTime > DurationSec;
    }

    public Vector2 GetSpeed()
    {
        return Speed * Dir;
    }

    public void OnStart()
    {
        StartAction?.Invoke();
    }
    public void OnRemove()
    {
        EndAction?.Invoke();
    }
}

