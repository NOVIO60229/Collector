﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BossLevelManager : MonoBehaviour
{
    public static Action OnLevelStart;
    public static Action OnLevelBossActive;
    public static Action OnLevelBossStartAttack;
    public static Action OnLevelEnd;
    public static Action OnLevelLeave;

    BPM_Counter BPM;

    [Header("Light Event")]
    public GameObject[] Lights;
    private int LightIndex = 0;

    private void Start()
    {
        BPM = BPM_Counter.instance;
        Timer(1f, OnLevelStart);
    }
    private void Update()
    {
        CheckEvents();
        if (Mathf.Abs(PlayerController.instance.transform.position.x - CameraController.instance._camera.transform.position.x) > 31)
        {
            OnLevelEnd?.Invoke();
        }
    }

    private void CheckEvents()
    {
        if (BPM._beatFullCount == 1)
        {
        }
        if (BPM._beatFullCount == 16)
        {
            OnLevelBossActive?.Invoke();
        }
        else if (BPM._beatFullCount == 32)
        {
            OnLevelBossStartAttack?.Invoke();
        }
        else if (BPM._beatFullCount == 55)
        {
            BossMoveTarget.Instance.PlayIdle();
        }
        else if (BPM._beatFullCount == 120)
        {
            CameraController.instance.ZoomOut();
        }
        else if (BPM._beatFullCount == 236)
        {
            BossStatus s = BossStatus.instance;
            s.transform.DOMove(s.transform.position + new Vector3(-1, -7, 0), 2f).SetEase(Ease.Linear);
        }
        else if (BPM._beatFullCount == 244)
        {
            BossStatus s = BossStatus.instance;
            s.SetIsFollow(false);
            BossRoom.Instance.RocksDown();
        }
        else if (BPM._beatFullCount == 260)
        {
            OnLevelEnd?.Invoke();
        }


        if (BPM._beatFullCount > 208)
        {
            BossStatus.instance.SetIsFollow(false);
        }
    }

    public void Timer(float time, Action EndOperation)
    {
        StartCoroutine(TimerCoroutine(time, EndOperation));
    }
    public IEnumerator TimerCoroutine(float time, Action EndOperation)
    {
        yield return new WaitForSeconds(time);
        EndOperation?.Invoke();
    }
}
