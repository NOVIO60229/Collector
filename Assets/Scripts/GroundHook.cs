﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundHook : MonoBehaviour
{
    public GameObject[] Particles;
    private float interval;

    BossRoom Room;
    private void Start()
    {
        interval = BPM_Counter.instance._beatIntervalSec;
        Room = BossRoom.Instance;
    }
    public void Play()
    {
        StartCoroutine(DoPlay());
    }
    IEnumerator DoPlay()
    {
        StartCoroutine(Shock(2, 0));
        StartCoroutine(Shock(2, 1));
        yield return new WaitForSeconds(2 * interval);
        StartCoroutine(Shock(1, 2));
        StartCoroutine(Shock(1, 3));
        yield return new WaitForSeconds(interval);
        StartCoroutine(Shock(1, 4));
        StartCoroutine(Shock(1, 5));
        yield return new WaitForSeconds(2 * interval);
        StartCoroutine(Shock(1, 6));
        yield return new WaitForSeconds(interval);
        Particles[7].SetActive(true);
        yield return new WaitForSeconds(interval);
        Particles[7].SetActive(false);
    }
    IEnumerator Shock(float waitBeat, int index)
    {
        WarningUtil.Instance.DrawBox(Particles[index].transform.position, Vector3.up, Room.VerticalDistance, 5f, interval);

        yield return new WaitForSeconds(waitBeat * interval);

        Particles[index].SetActive(true);
        yield return new WaitForSeconds(interval);
        Particles[index].SetActive(false);
    }
}
