﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;
using Cinemachine;
using UnityEngine.InputSystem;
using System.Threading;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour, IDamagable, IBounce
{
    [HideInInspector] public State state = State.normal;

    public static PlayerController instance;

    //components
    [HideInInspector] public Rigidbody2D Rb;
    SpriteRenderer SpriRen;
    BoxCollider2D BoxCollider;
    PlayerAnimation PlayerAnimation;
    CinemachineImpulseSource Impulse;
    PlayerStatus Status;

    //player state
    [HideInInspector] public bool IsGrounded = false;
    [HideInInspector] public bool IsHurt = false;
    [HideInInspector] public bool CanNotMove = false;
    private bool IsInvincible = false;
    public PlayerControllerInput InputThisFrame;
    [HideInInspector] public Vector2 FacingDir = Vector2.right;

    //run variables
    [HideInInspector] public Vector2 InputDir = Vector2.zero;

    //Damage variable
    public int HP { get; set; } = 100;
    private float KnockBackForce = 40f;
    private bool IsKnocked = false;
    private Vector2 KnockDir = Vector2.zero;
    private float HurtTime = 0.3f;
    private bool IsDead = false;

    //jump variable
    private bool IsJumpButtonDown = false;
    private bool IsJumpButtomPressed = false;
    private float JumpForce = 30f;
    public LayerMask GroundLayers;
    private float FallMultiplier = 4.5f;
    private float LowJumpMultiplier = 15f;
    private float LastJumpMultiplier = 7f;
    private int JumpTimes = 1;
    private int JumpTimeLimit = 1;
    private float BounceForce = 60f;

    //slide variables
    private float SlideTime = 0.45f;
    private float SlideCurrentTime = 0f;
    private float SlideInvincibleTimePercent = 0.1f;
    private bool IsSlideCD = false;
    private float SlideCDTime = 0.6f;

    private float SlideSpeedStartBoost = 35f;
    private float SlideSpeedStartBoostSec = 1.5f;
    private float SlideSpeedContinuousBoost = 40f;
    //private float SlideSpeedContinuousBoostSec = 0.01f;
    //private float SlideSpeedContinuousBoostIntervalSec = 0.01f;
    private float LastSlideBoostTime;

    private bool IsAniFromSlideToIdle = false;
    private bool IsAbleCancelSlide = false;

    //crouch variables
    private bool IsCrouchButtonDown = false;
    private bool IsCrouchButtonPressed = false;
    private readonly float Normal_OffsetY = -0.1531262f;
    private readonly float Normal_SizeY = 5.273747f;
    private readonly float Crouch_OffsetY = -1.541023f;
    private readonly float Crouch_SizeY = 2.497954f;
    private readonly float Slide_OffsetY = -2.133437f;
    private readonly float Slide_SizeY = 1.313127f;

    //backpack variables
    private BackPack BackpackComponent;
    public Transform ThrowOriginalTransform;
    public Transform JumpThrowOriginalTransform;

    //platformCheck
    public Transform PlayerGroundedPoint;

    [HideInInspector] public bool IsAnimatingThrow = false;
    [HideInInspector] public bool IsAnimatingJumpThrow = false;
    [HideInInspector] public bool IsBackpackOnBack = true;



    private void Awake()
    {
        instance = this;
    }
    private void OnEnable()
    {
    }
    private void OnDisable()
    {
    }

    private void Start()
    {
        Rb = GetComponent<Rigidbody2D>();
        SpriRen = GetComponent<SpriteRenderer>();
        BoxCollider = GetComponent<BoxCollider2D>();
        PlayerAnimation = GetComponent<PlayerAnimation>();
        Impulse = GetComponent<CinemachineImpulseSource>();
        Status = GetComponent<PlayerStatus>();
        SetUpBackPack();
    }

    void SetUpBackPack()
    {
        BackpackComponent = Instantiate(PrefabHandler.Instance.BackPack).GetComponent<BackPack>();
        BackpackComponent.SetUp(ThrowOriginalTransform, JumpThrowOriginalTransform);
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        InputDir = context.ReadValue<Vector2>();
    }
    public void OnJump(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            IsJumpButtonDown = true;
            IsJumpButtomPressed = true;
        }
        if (context.canceled)
        {
            IsJumpButtomPressed = false;
        }
    }
    public void OnCrouch(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            IsCrouchButtonDown = true;
            IsCrouchButtonPressed = true;
            Crouch();
        }
        if (context.canceled)
        {
            IsCrouchButtonPressed = false;
        }
    }
    void Crouch()
    {
        if (!IsGrounded) return;

        if (IsCrouchButtonPressed && InputDir.x == 0)
        {
            state = State.crouch;
            CancelThrowAni();
        }
        else if (!IsSlideCD && state == State.normal)
        {
            Slide();
            CancelThrowAni();
        }
    }
    private void Slide()
    {
        IsSlideCD = true;
        state = State.slide;
        SetColliderSlideMode();
        SlideCurrentTime = 0;
        Status.AddMoveSpeed(SlideSpeedStartBoost, SlideSpeedStartBoostSec);
        LastSlideBoostTime = Time.time;
        PlayerAnimation.PlayAnimationOnce("Slide");

        IsAbleCancelSlide = false;
        Timer(PlayerAnimation._aniSecDic["Slide"], () => IsAbleCancelSlide = true);
    }
    void CancelThrowAni()
    {
        IsAnimatingJumpThrow = false;
        IsAnimatingThrow = false;
    }


    public void OnThrow(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            ThrowBackpack();
        }
    }
    void ThrowBackpack()
    {
        if (state == State.normal)
        {
            BackpackComponent.Trigger();
        }
    }
    private void Update()
    {
        CheckGrounded();
        RecordInputAndState();
        AdjustGravityForJump();

        switch (state)
        {
            case State.normal:
                UpdateAnimation();
                CheckFlip();
                break;

            case State.slide:
                SlideUpdate();
                break;
            case State.crouch:
                CrouchUpdate();
                break;
            default:
                Debug.LogError("Not in any state");
                break;
        }
    }
    private void CheckGrounded()
    {
        Collider2D hit = Physics2D.OverlapBox(
            (Vector2)BoxCollider.bounds.center - new Vector2(0, BoxCollider.bounds.extents.y + 0.1f), new Vector2(BoxCollider.bounds.size.x * 0.9f, 0.1f), 0, GroundLayers);
        bool hitSolid = hit ? true : false;
        IsGrounded = hitSolid && Rb.velocity.y <= 0;
        if (IsGrounded)
        {
            ResetJumpTimes();
        }
    }
    public void ResetJumpTimes()
    {
        JumpTimes = JumpTimeLimit;
    }
    void RecordInputAndState()
    {
        InputThisFrame = new PlayerControllerInput(transform.position, Rb.velocity, InputDir,
            IsJumpButtonDown, IsJumpButtomPressed, IsCrouchButtonDown, IsCrouchButtonPressed);
    }

    private void AdjustGravityForJump()
    {
        if (!IsGrounded)
        {
            if (Rb.velocity.y < 0)
            {
                Rb.velocity += Vector2.up * Physics2D.gravity.y * FallMultiplier * Time.deltaTime;
            }
            //add extra force on the last jump if holding button
            else if (Rb.velocity.y > 0 && JumpTimes == 0 && IsJumpButtomPressed)
            {
                Rb.velocity += Vector2.up * Physics2D.gravity.y * LastJumpMultiplier * Time.deltaTime;
            }
            else if (Rb.velocity.y > 0)
            {
                Rb.velocity += Vector2.up * Physics2D.gravity.y * LowJumpMultiplier * Time.deltaTime;
            }
        }
    }
    private void CheckFlip()
    {
        if (InputDir.x < 0)
        {
            FacingDir = Vector2.left;
            transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        }
        else if (InputDir.x > 0)
        {
            FacingDir = Vector2.right;
            transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        }
    }
    void SlideUpdate()
    {
        CheckInvincible();
        CheckInteruptSlide();
    }
    void CheckInteruptSlide()
    {
        if (Status.SlideSpeed <= PlayerStatus.BaseCrouchSpeed)
        {
            state = State.crouch;
            Timer(SlideCDTime, () => IsSlideCD = false);
        }
        else if (IsToNormalColliderStuck())
        {
            return;
        }
        else if (!IsCrouchButtonPressed && IsAbleCancelSlide)
        {
            state = State.normal;
            SetColliderNormalMode();
            IsAniFromSlideToIdle = true;
            Timer(PlayerAnimation._aniSecDic["SlideToIdle"], () => IsAniFromSlideToIdle = false);
            Timer(SlideCDTime, () => IsSlideCD = false);
        }
        else if (IsJumpButtonDown)
        {
            state = State.normal;
            SetColliderNormalMode();
            Timer(SlideCDTime, () => IsSlideCD = false);
        }
    }
    void SetColliderSlideMode()
    {
        BoxCollider.offset = new Vector2(BoxCollider.offset.x, Slide_OffsetY);
        BoxCollider.size = new Vector2(BoxCollider.size.x, Slide_SizeY);
    }
    void SetColliderCrouchMode()
    {
        BoxCollider.offset = new Vector2(BoxCollider.offset.x, Crouch_OffsetY);
        BoxCollider.size = new Vector2(BoxCollider.size.x, Crouch_SizeY);
    }
    void SetColliderNormalMode()
    {
        BoxCollider.offset = new Vector2(BoxCollider.offset.x, Normal_OffsetY);
        BoxCollider.size = new Vector2(BoxCollider.size.x, Normal_SizeY);
    }
    bool IsToNormalColliderStuck()
    {
        SetColliderNormalMode();
        Collider2D hit = Physics2D.OverlapBox(BoxCollider.bounds.center, new Vector2(BoxCollider.bounds.size.x * 0.9f, BoxCollider.bounds.size.y * 0.9f), 0, LayerMask.GetMask("Solid"));
        bool IsStuck = hit ? true : false;
        SetColliderSlideMode();
        return IsStuck;
    }
    void CheckInvincible()
    {
        SlideCurrentTime += Time.deltaTime;
        if (SlideCurrentTime < SlideTime * SlideInvincibleTimePercent)
        {
            IsInvincible = true;
        }
        else
        {
            IsInvincible = false;
        }
    }
    void CrouchUpdate()
    {
        CheckInteruptCrouch();
        PlayCrouchAnimation();
        CheckFlip();
    }
    void CheckInteruptCrouch()
    {
        if (IsToNormalColliderStuck()) return;

        if (IsJumpButtonDown || !IsCrouchButtonPressed)
        {
            state = State.normal;
            SetColliderNormalMode();
        }
    }
    void PlayCrouchAnimation()
    {
        if (InputDir.x != 0)
        {
            PlayerAnimation.PlayAnimation("Crouch");
        }
        else
        {
            PlayerAnimation.PlayAnimation("CrouchIdle");
        }
    }
    private void LateUpdate()
    {
        ClearInput();
    }
    private void ClearInput()
    {
        IsCrouchButtonDown = false;
    }
    private void FixedUpdate()
    {
        if (IsKnocked)
        {
            Rb.velocity = KnockDir * KnockBackForce;
            IsKnocked = false;
        }

        switch (state)
        {
            case State.normal:

                Rb.velocity = new Vector2(InputDir.x * Status.RunSpeed, Rb.velocity.y);
                break;

            case State.slide:

                Rb.velocity = new Vector2(FacingDir.x * Status.SlideSpeed, Rb.velocity.y);

                //if (Time.time > LastSlideBoostTime + SlideSpeedContinuousBoostIntervalSec)
                //{
                //    Status.AddMoveSpeed(SlideSpeedContinuousBoost, SlideSpeedContinuousBoostSec, Ease.Linear);
                //    LastSlideBoostTime = Time.time;
                //}
                break;

            case State.crouch:

                Rb.velocity = new Vector2(InputDir.x * Status.CrouchSpeed, Rb.velocity.y);
                break;

            default:

                break;
        }


        if (IsJumpButtonDown)
        {
            if (state == State.slide && IsToNormalColliderStuck())
            {
                return;
            }

            Jump();
        }

        ApplyForce();

        SnapToGround();
    }
    void ApplyForce()
    {
        Vector2 force = Status.Force;
        if (force == Vector2.zero || force.magnitude < Status.RunSpeed) return;

        Rb.velocity = new Vector2(Rb.velocity.x * 0.5f, 0) + force;
    }
    void SnapToGround()
    {
        if (IsGrounded && Rb.velocity.y < 0)
        {
            Rb.velocity = new Vector2(Rb.velocity.x, 0);
        }
    }

    void UpdateAnimation()
    {
        if (IsAnimatingJumpThrow && IsGrounded)
        {
            IsAnimatingJumpThrow = false;
        }

        if (IsDead)
        {
            PlayerAnimation.PlayAnimation("Dead");
        }
        else if (IsHurt)
        {
            PlayerAnimation.PlayAnimation("Hurt");
        }
        else if (IsAnimatingJumpThrow)
        {
            PlayerAnimation.PlayAnimation("JumpThrow");
        }
        else if (IsAnimatingThrow)
        {
            PlayerAnimation.PlayAnimation("Throw");
        }
        else if (Rb.velocity.y > 0.1f)
        {
            PlayerAnimation.PlayAnimation("JumpUp");
        }
        else if (Rb.velocity.y < -0.1f && !IsGrounded)
        {
            PlayerAnimation.PlayAnimation("JumpDown");
        }
        else if (IsAniFromSlideToIdle)
        {
            PlayerAnimation.PlayAnimation("SlideToIdle");
        }
        else if (InputDir.x != 0)
        {
            PlayerAnimation.Play_Run(Status.RunSpeed);
        }
        else
        {
            PlayerAnimation.PlayAnimation("Idle");
        }

    }

    void Jump()
    {
        IsJumpButtonDown = false;
        if (JumpTimes == 0) return;

        Rb.velocity = new Vector2(Rb.velocity.x, JumpForce);
        JumpTimes -= 1;

        Status.ClearForce();
    }

    public void ResetPlayerState()
    {
        Status.ClearForce();
        Rb.velocity = Vector2.zero;
        BackpackComponent.PickUp();
    }

    public void Hurt(int damage, Transform attacker)
    {
        if (IsHurt || IsInvincible) return;

        Status.AddMoveSpeed(-10, 1f);

        //vibrate
        if (Gamepad.current != null)
        {
            Gamepad.current.SetMotorSpeeds(0.5f, 0.5f);
            Timer(0.2f, () => Gamepad.current.SetMotorSpeeds(0, 0f));
        }

        HP -= damage;
        if (HP <= 0)
        {
            IsDead = true;
            GetComponent<PlayerInput>().enabled = false;
            Timer(2, () =>
             {
                 SceneCutter.Instance.LoadScene(SceneManager.GetActiveScene().name, 2f);
             });
        }

        IsKnocked = true;
        //knock direction depends on position of attacker
        KnockDir = attacker.position.x > transform.position.x ? new Vector2(-1, 0.2f) : new Vector2(1, 0.2f);

        StartCoroutine(HurtReaction());
    }

    public IEnumerator HurtReaction()
    {
        IsHurt = true;
        yield return Blinking(HurtTime);
        IsHurt = false;
    }

    public IEnumerator Blinking(float duration)
    {
        float timeLeft = duration;

        //shine red light effect variables
        float switchColorTime = 0.08f;
        float colorTime = switchColorTime;
        bool isColorRed = true;

        while (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;

            //shine red light effect
            colorTime -= Time.deltaTime;
            if (colorTime <= 0)
            {
                colorTime += switchColorTime;
                isColorRed = !isColorRed;

                if (isColorRed)
                    SpriRen.color = Color.red;
                else
                    SpriRen.color = Color.white;
            }
            yield return null;
        }
        SpriRen.color = Color.white;
    }


    public void Bounce()
    {
        if (state == State.slide) return;
        Rb.velocity = new Vector2(Rb.velocity.x, BounceForce);
        ResetJumpTimes();
        BackpackComponent.PickUp();
        //Gamepad.current.SetMotorSpeeds(0.5f, 0.5f);
        //Timer(0.2f, () => Gamepad.current.SetMotorSpeeds(0, 0f));
    }

    public void Timer(float time, Action EndOperation)
    {
        StartCoroutine(TimerCoroutine(time, EndOperation));
    }
    public IEnumerator TimerCoroutine(float time, Action EndOperation)
    {
        yield return new WaitForSeconds(time);
        EndOperation?.Invoke();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        TrapHolder trap = collision.GetComponent<TrapHolder>();
        trap?.ExecuteEnter();
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        TrapHolder trap = collision.GetComponent<TrapHolder>();
        trap?.ExecuteStay();
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        TrapHolder trap = collision.GetComponent<TrapHolder>();
        trap?.ExecuteExit();
    }
}



