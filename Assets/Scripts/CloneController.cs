﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;
using Cinemachine;
using UnityEngine.InputSystem;
using System.Threading;

public class CloneController : MonoBehaviour, IBounce
{
    public enum State
    {
        normal,
        slide,
        crouch,
        hurt
    }
    [HideInInspector] public State state = State.normal;

    //components
    [HideInInspector] public Rigidbody2D Rb;
    SpriteRenderer SpriRen;
    BoxCollider2D BoxCollider;
    PlayerAnimation PlayerAnimation;
    PlayerControllerRecord PlayerControllerRecord;

    //player state
    private int InputRecordIndex = 0;
    private bool IsActivated = false;
    [HideInInspector] public bool IsGrounded = false;
    [HideInInspector] public Vector2 FacingDir = Vector2.right;

    //run variables
    [HideInInspector] public Vector2 InputDir = Vector2.zero;
    [HideInInspector] public float RunSpeed = 20f;
    private float RestrictedLerpRate = 0.4f;
    public LayerMask ScreenWallLayer;

    //jump variable
    private bool IsJumpButtonDown = false;
    private bool IsJumpButtomPressed = false;
    private float JumpForce = 45f;
    public LayerMask GroundLayers;
    private float FallMultiplier = 4.5f;
    private float LowJumpMultiplier = 15f;
    private float LastJumpMultiplier = 7f;
    private int JumpTimes = 1;
    private int JumpTimeLimit = 1;


    //dash variables
    private bool IsDashButtonDown = false;
    private float DashForce = 3f;
    public LayerMask DashLayerMask;
    private bool IsDashCD = false;
    private float DashCDTime = 0.5f;

    //slide variables
    private float SlideSpeedMax = 40f;
    private float SlideSpeedMin = 25f;
    private float SlideCurrentSpeed = 0f;
    private float SlideTime = 0.45f;
    private float SlideCurrentTime = 0f;
    private float SlideInvincibleTimePercent = 0.1f;
    private bool IsSlideCD = false;
    private float SlideCDTime = 0f;

    //crouch variables
    private bool IsCrouchButtonDown = false;
    private bool IsCrouchButtonPressed = false;
    private readonly float Normal_OffsetY = -0.1531262f;
    private readonly float Normal_SizeY = 5.273747f;
    private readonly float Crouch_OffsetY = -2.133437f;
    private readonly float Crouch_SizeY = 1.313127f;

    //backpack variables
    public GameObject BackpackPrefab;
    BackPack BackpackComponent;
    private float IgnoreCollisionTime = 0.4f;
    private float ThrowForce = 32f;
    private float BounceForce = 20f;
    [HideInInspector] public bool IsAnimatingThrow = false;
    [HideInInspector] public bool IsAnimatingJumpThrow = false;
    [HideInInspector] public bool IsBackpackOnBack = true;

    private void Awake()
    {
    }
    private void OnEnable()
    {
    }
    private void OnDisable()
    {
    }

    private void Start()
    {
        Rb = GetComponent<Rigidbody2D>();
        SpriRen = GetComponent<SpriteRenderer>();
        BoxCollider = GetComponent<BoxCollider2D>();
        PlayerAnimation = GetComponent<PlayerAnimation>();
        BackpackComponent = BackpackPrefab.GetComponent<BackPack>();
        PlayerControllerRecord = PlayerControllerRecord.Instance;
    }
    public void Activate(float secAgo)
    {
        SetStartTimePoint(secAgo);
        IsActivated = true;
        IgnoreCollisionWithPlayer();
    }

    void Crouch()
    {
        IsCrouchButtonDown = false;
        if (!IsGrounded) return;

        if (InputDir.x == 0)
        {
            //crouch
        }
        else if (!IsSlideCD)
        {
            state = State.slide;
            SlideCurrentSpeed = SlideSpeedMax;
            SlideCurrentTime = 0;
            IsSlideCD = true;
            DOTween.To(() => SlideCurrentSpeed, x => SlideCurrentSpeed = x, SlideSpeedMin, SlideTime).onComplete += () =>
            {
                if (state != State.normal)
                {
                    state = State.normal;
                    SetColliderNormalMode();
                    Timer(SlideCDTime, () => IsSlideCD = false);
                }
            };
        }
    }
    private void SetStartTimePoint(float sec)
    {
        InputRecordIndex = PlayerControllerRecord.GetIndexOfInputSecAgo(sec);
    }
    private void IgnoreCollisionWithPlayer()
    {
        Physics2D.IgnoreCollision(BoxCollider, PlayerController.instance.GetComponent<BoxCollider2D>());
    }

    private void Update()
    {
        SetInput();
        CheckGrounded();

        if (IsCrouchButtonDown)
        {
            Crouch();
        }

        switch (state)
        {
            case State.normal:

                UpdateAnimation();
                AdjustGravityForJump();
                CheckFlip();
                break;

            case State.slide:

                PlayerAnimation.PlayAnimation("Slide", SlideTime);
                SetColliderSlideMode();

                // jump to interupt slide
                if (IsJumpButtonDown)
                {
                    state = State.normal;
                    SetColliderNormalMode();
                    Timer(SlideCDTime, () => IsSlideCD = false);
                }
                break;
            default:
                Debug.LogError("Not in any state");
                break;

        }
    }
    private void SetInput()
    {
        if (!IsActivated) return;

        PlayerControllerInput input = PlayerControllerRecord.InputList[InputRecordIndex];
        InputRecordIndex++;

        transform.position = input.Position;
        //Rb.velocity = input.Velocity;
        InputDir = input.InputDir;
        IsCrouchButtonDown = input.IsCrouchDown;
        IsCrouchButtonPressed = input.IsCrouchPressed;
        IsJumpButtomPressed = input.IsJumpButtomPressed;
        IsJumpButtonDown = input.IsJumpButtonDown;
    }

    private void CheckGrounded()
    {
        // isGrounded check
        Collider2D hit = Physics2D.OverlapBox(
            (Vector2)BoxCollider.bounds.center - new Vector2(0, BoxCollider.bounds.extents.y + 0.01f), new Vector2(BoxCollider.bounds.size.x * 0.9f, 0.01f), 0, GroundLayers);
        IsGrounded = hit ? true : false;

        if (IsGrounded)
        {
            JumpTimes = JumpTimeLimit;
        }
    }
    private void AdjustGravityForJump()
    {
        if (!IsGrounded)
        {
            if (Rb.velocity.y < 0)
            {
                Rb.velocity += Vector2.up * Physics2D.gravity.y * FallMultiplier * Time.deltaTime;
            }
            //add extra force on the last jump if holding button
            else if (Rb.velocity.y > 0 && JumpTimes == 0 && IsJumpButtomPressed)
            {
                Rb.velocity += Vector2.up * Physics2D.gravity.y * LastJumpMultiplier * Time.deltaTime;
            }
            else if (Rb.velocity.y > 0)
            {
                Rb.velocity += Vector2.up * Physics2D.gravity.y * LowJumpMultiplier * Time.deltaTime;
            }
        }
    }
    private void CheckFlip()
    {
        if (InputDir.x < 0)
        {
            FacingDir = Vector2.left;
            transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        }
        else if (InputDir.x > 0)
        {
            FacingDir = Vector2.right;
            transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        }
    }

    void SetColliderSlideMode()
    {
        BoxCollider.offset = new Vector2(BoxCollider.offset.x, Crouch_OffsetY);
        BoxCollider.size = new Vector2(BoxCollider.size.x, Crouch_SizeY);
    }
    void SetColliderNormalMode()
    {
        BoxCollider.offset = new Vector2(BoxCollider.offset.x, Normal_OffsetY);
        BoxCollider.size = new Vector2(BoxCollider.size.x, Normal_SizeY);
    }


    private void FixedUpdate()
    {
        switch (state)
        {
            case State.normal:

                float speed = InputDir.x * RunSpeed;

                if (IsDashButtonDown)
                {
                    if (!IsDashCD)
                        Dash();
                    IsDashButtonDown = false;
                }

                if (IsJumpButtonDown)
                {
                    Jump();
                }
                break;

            case State.slide:

                Rb.velocity = new Vector2(FacingDir.x * SlideCurrentSpeed, Rb.velocity.y);
                break;

            default:
                Debug.LogError("Not in any state");
                break;
        }
    }

    void UpdateAnimation()
    {
        if (IsAnimatingJumpThrow && IsGrounded)
        {
            IsAnimatingJumpThrow = false;
            IsAnimatingThrow = false;
        }

        if (IsAnimatingThrow)
        {
            if (IsAnimatingJumpThrow)
            {
                PlayerAnimation.PlayAnimation("JumpThrow");
            }
            else
            {
                PlayerAnimation.PlayAnimation("Throw");
            }
        }
        else
        {
            if (Rb.velocity.y > 0.1f)
            {
                PlayerAnimation.PlayAnimation("JumpUp");
            }
            else if (Rb.velocity.y < -0.1f)
            {
                PlayerAnimation.PlayAnimation("JumpDown");
            }
            else if (InputDir.x != 0)
            {
                PlayerAnimation.PlayAnimation("Run");
            }
            else
            {
                PlayerAnimation.PlayAnimation("Idle");
            }
        }

    }
    void Dash()
    {
        Vector2 DesirePosition = (Vector2)transform.position + (InputDir.normalized * DashForce);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, InputDir.normalized, DashForce, DashLayerMask);
        if (hit)
        {
            DesirePosition = hit.point;
        }

        //dash effect

        Rb.MovePosition(DesirePosition);
        IsDashButtonDown = false;
        IsDashCD = true;
        Timer(DashCDTime, () => IsDashCD = false);
    }

    void Jump()
    {
        IsJumpButtonDown = false;
        if (JumpTimes == 0 || Rb.velocity.y > 0) return;

        Rb.velocity = new Vector2(0, JumpForce);
        JumpTimes -= 1;

        //CameraHolder.instance.Shake();
    }

    public void Bounce()
    {
        //if (state == State.slide) return;
        //Rb.velocity = new Vector2(Rb.velocity.x, BounceForce);
        //Gamepad.current.SetMotorSpeeds(0.5f, 0.5f);
        //Timer(0.2f, () => Gamepad.current.SetMotorSpeeds(0, 0f));
    }

    public void ThrowBackPack()
    {
        BackpackComponent.Trigger();
    }

    public void Timer(float time, Action EndOperation)
    {
        StartCoroutine(TimerCoroutine(time, EndOperation));
    }
    public IEnumerator TimerCoroutine(float time, Action EndOperation)
    {
        yield return new WaitForSeconds(time);
        EndOperation?.Invoke();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        TrapHolder trap = collision.GetComponent<TrapHolder>();
        trap?.ExecuteEnter();
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        TrapHolder trap = collision.GetComponent<TrapHolder>();
        trap?.ExecuteStay();
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        TrapHolder trap = collision.GetComponent<TrapHolder>();
        trap?.ExecuteExit();
    }

}
