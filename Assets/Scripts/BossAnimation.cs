﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAnimation : MonoBehaviour
{
    public static BossAnimation Instance;

    [HideInInspector] public Animator Animator;
    [HideInInspector] public string StateName = "Idle";
    private float NormalizedTime = 0;
    private float PlayTimeMultiplier = 1;
    private float BeatSec = 0;


    public GameObject chargeParticle;
    public ParticleSystem ElectricityParticle;



    public Dictionary<string, float> _aniSecDic = new Dictionary<string, float>() {
        {"Idle",1f},{ "Alt",1f},{"Shoot",2f},{"Attack" ,0.2f},{"Shield", 1f},{"Charge",0.8f},{"Charging", 0.8f},{"Move",0.8f}
    };

    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        Animator = GetComponent<Animator>();
        BeatSec = BPM_Counter.instance._beatIntervalSec;
    }
    private void Update()
    {
        //
    }

    public void PlayCharge()
    {
        chargeParticle.SetActive(true);
        Animator.Play("Charge", 0, 0);
    }
    public void EndCharge()
    {
        chargeParticle.SetActive(false);
        Animator.Play("Idle", 0, 0);
    }

    public void PlayElectricity()
    {
        ElectricityParticle.Play();
    }


    public void PlayOnce(string aniName, float playSecond)
    {
        ChangeStateNameAndPlayrate(aniName, playSecond);
    }
    public void PlayOnce(string aniName)
    {
        ChangeStateNameAndPlayrate(aniName, _aniSecDic[aniName]);
    }
    public void ChageTo(string aniName, float playSecond)
    {
        if (aniName == StateName) return;
        ChangeStateNameAndPlayrate(aniName, playSecond);
    }
    public void ChageTo(string aniName)
    {
        if (aniName == StateName) return;
        ChangeStateNameAndPlayrate(aniName, _aniSecDic[aniName]);
    }
    //public void Play_Animation(string transition, string aniName, float transitionPlaySecond, float playSecond)
    //{
    //    if (transition == StateName || aniName == StateName) return;
    //    StartCoroutine(StartTransition(transition, aniName, transitionPlaySecond, playSecond));
    //}
    //public void Play_Animation(string transition, string aniName)
    //{
    //    if (transition == StateName || aniName == StateName) return;
    //    StartCoroutine(StartTransition(transition, aniName, _aniSecDic[transition], _aniSecDic[aniName]));
    //}

    public void ChangeStateNameAndPlayrate(string name, float _playTime)
    {
        StateName = name;
        PlayTimeMultiplier = 1 / _playTime;

        NormalizedTime = 0;
    }
}
