﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerControllerRecord : MonoBehaviour
{
    public static PlayerControllerRecord Instance;
    public List<PlayerControllerInput> InputList = new List<PlayerControllerInput>();
    public int ListMaximum = 30000;
    private const int RemoveInputAmout = 2000;

    PlayerController Player;

    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        Player = PlayerController.instance;
    }
    private void Update()
    {
        RecordInput();
        ClearWhenExceed();
    }

    private void RecordInput()
    {
        InputList.Add(Player.InputThisFrame);
    }
    private void ClearWhenExceed()
    {
        if (InputList.Count > ListMaximum)
        {
            InputList.RemoveRange(0, RemoveInputAmout);
        }
    }
    public bool IsRecordMoreThanSec(float sec)
    {
        if (InputList.Count > sec * (1 / Time.deltaTime))
        {
            return true;
        }
        return false;
    }
    public int GetIndexOfInputSecAgo(float sec)
    {
        int index = InputList.Count - 1 - (int)(sec * 1 / Time.deltaTime);
        if (index < 0)
        {
            index = 0;
        }
        return index;
    }
    public void Clear()
    {
        InputList.Clear();
    }
}

public class PlayerControllerInput
{
    public Vector3 Position;
    public Vector2 Velocity;

    public Vector2 InputDir = Vector2.zero;
    public bool IsJumpButtonDown = false;
    public bool IsJumpButtomPressed = false;
    public bool IsCrouchDown = false;
    public bool IsCrouchPressed = false;

    public PlayerControllerInput(Vector3 position, Vector2 velocity, Vector2 inputDir, bool isJumpButtonDown, bool isJumpButtomPressed, bool isCrouchDown, bool isCrouchPressed)
    {
        Position = position;
        Velocity = velocity;
        InputDir = inputDir;
        IsCrouchDown = isCrouchDown;
        IsCrouchPressed = isCrouchPressed;
        IsJumpButtomPressed = isJumpButtomPressed;
        IsJumpButtonDown = isJumpButtonDown;
    }
}
