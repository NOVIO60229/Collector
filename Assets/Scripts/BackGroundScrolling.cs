﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundScrolling : MonoBehaviour
{
    public Transform TrackTarget;
    public List<BackGroundMovingData> BackGroundList = new List<BackGroundMovingData>();

    private Vector3 TargetLastPosition;
    private Vector2 Movement;
    void Start()
    {
        TargetLastPosition = TrackTarget.position;
    }

    // Update is called once per frame
    void Update()
    {
        DetectTrackMovement();
        Move();
    }

    void DetectTrackMovement()
    {
        Movement = TrackTarget.position - TargetLastPosition;
        TargetLastPosition = TrackTarget.position;
    }
    void Move()
    {
        foreach (BackGroundMovingData data in BackGroundList)
        {
            data.Target.transform.position -= new Vector3(Movement.x * data.MoveMultiplierX, Movement.y * data.MoveMultiplierY, 0);
        }
    }
}
[System.Serializable]
public class BackGroundMovingData
{
    public GameObject Target;
    public float MoveMultiplierX = 0.1f;
    public float MoveMultiplierY = 0;
}
