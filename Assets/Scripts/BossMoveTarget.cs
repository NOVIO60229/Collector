﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossMoveTarget : MonoBehaviour
{
    public static BossMoveTarget Instance;
    public Animator animator;

    void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        animator = GetComponent<Animator>();
    }
    public void PlayIdle()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            return;
        }
        animator.Play("Idle", 0, 0);
    }
}
