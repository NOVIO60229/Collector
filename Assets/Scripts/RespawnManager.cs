﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnManager : MonoBehaviour
{
    public static RespawnManager Instance;
    private Vector2 CurrentCheckPoint;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }

        Instance = this;
    }
    void Start()
    {
    }

    // Update is called once per frame

    public void Die()
    {
        Respawn();
    }
    public void Respawn()
    {
        PlayerController player = PlayerController.instance;
        player.gameObject.transform.position = CurrentCheckPoint;
        player.ResetPlayerState();
    }
    public void UpdateCheckPoint(Vector2 point)
    {
        CurrentCheckPoint = point;
    }
}
