﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using DG.Tweening.Plugins.Core.PathCore;

public class Platform : MonoBehaviour, Triggerable
{
    public enum MovingMode { Trigger, Auto };
    public MovingMode movingMode;

    public List<MovingPath> PathList = new List<MovingPath>();

    //moving related
    protected bool IsMoving;
    protected Vector3 origin;
    protected Sequence seq;

    void Start()
    {
        origin = transform.position;

        //CheckEmptyValue
        if (PathList.Count == 0)
        {
            print(gameObject + " has empty value of PlatformMoving");
            return;
        }

        SetMovePattern();
    }
    void SetMovePattern()
    {
        float SecPerBeat = BPM_Counter.instance._beatIntervalSec;
        seq = DOTween.Sequence();
        Tween t;
        for (int i = 0; i < PathList.Count; i++)
        {
            if (PathList[i].DelayBeat != 0)
            {

                t = transform.DOMove(origin + PathList[i].Offset, PathList[i].Beat * SecPerBeat).SetDelay(PathList[i].DelayBeat * SecPerBeat).SetEase(PathList[i].Ease);
            }
            else
            {
                t = transform.DOMove(origin + PathList[i].Offset, PathList[i].Beat * SecPerBeat).SetEase(PathList[i].Ease);
            }

            if (movingMode == MovingMode.Trigger && i == PathList.Count - 1)
            {
                t.onComplete += () =>
                {
                    seq.Pause();
                    IsMoving = false;
                };
                seq.Pause();
            }

            seq.Append(t);
        }
        seq.SetLoops(-1);
    }

    public virtual void Trigger()
    {
        switch (movingMode)
        {
            case MovingMode.Trigger:

                if (!IsMoving)
                {
                    IsMoving = true;
                    seq.Restart();
                    seq.Play();
                }
                break;

            case MovingMode.Auto:
                break;
        }
    }

    void ShowActiveEffect()
    {

    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        PlayerController controller = collision.gameObject.GetComponent<PlayerController>();

        if (collision.transform.tag == "Player")
        {
            if (controller.IsGrounded == true && controller.PlayerGroundedPoint.position.y > transform.position.y)
            {
                controller.transform.SetParent(transform);
            }
            else
            {
                controller.transform.SetParent(null);
            }

        }
        else if(collision.transform.tag == "Backpack")
        {
            collision.transform.SetParent(transform);
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        collision.collider.transform.SetParent(null);
    }
}

[System.Serializable]
public class MovingPath
{
    public Vector3 Offset = Vector3.zero;
    public Ease Ease = Ease.InOutQuad;
    public float Beat = 1;
    public float DelayBeat = 0;
}