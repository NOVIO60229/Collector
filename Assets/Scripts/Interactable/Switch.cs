﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Switch : MonoBehaviour
{
    public UnityEvent StartUnityEvent;
    public UnityEvent ResetUnityEvent;
    public float ResetBeat = 1f;

    private Animator Animator;
    private Collider2D Collider;

    private void Start()
    {
        Animator = GetComponent<Animator>();
        Collider = GetComponent<Collider2D>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" || collision.tag == "Backpack")
        {
            StartCoroutine(SwitchStart());
        }
    }

    IEnumerator SwitchStart()
    {
        Collider.enabled = false;
        Animator.Play("RedToGreen");
        StartUnityEvent?.Invoke();
        ShowTriggerEffect();

        yield return new WaitForSeconds(BPM_Counter.instance._beatIntervalSec * ResetBeat);
        SwitchReset();
    }
    void ShowTriggerEffect()
    {

    }

    void SwitchReset()
    {
        Collider.enabled = true;
        Animator.Play("GreenToRed");
        ResetUnityEvent?.Invoke();
        ShowResetEffect();
    }
    void ShowResetEffect()
    {

    }
}