﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using DG.Tweening.Plugins.Core.PathCore;

public class ShrinkPlatform : Platform
{
    public enum ShrinkMode { Trigger, Auto };
    public ShrinkMode shrinkMode;

    //shrink related
    private Animator Animator;
    private BoxCollider2D Collider;
    private bool IsShrinking = false;
    public float ShrinkResetBeat = 2f;


    void Start()
    {
        Animator = GetComponent<Animator>();
        Collider = GetComponent<BoxCollider2D>();
        origin = transform.position;

        //CheckEmptyValue
        if (PathList.Count == 0)
        {
            return;
        }

        SetMovePattern();
    }
    void SetMovePattern()
    {
        float SecPerBeat = BPM_Counter.instance._beatIntervalSec;
        seq = DOTween.Sequence();
        Tween t;

        for (int i = 0; i < PathList.Count; i++)
        {
            if (PathList[i].DelayBeat != 0)
            {

                t = transform.DOMove(origin + PathList[i].Offset, PathList[i].Beat * SecPerBeat).SetDelay(PathList[i].DelayBeat * SecPerBeat).SetEase(PathList[i].Ease);
            }
            else
            {
                t = transform.DOMove(origin + PathList[i].Offset, PathList[i].Beat * SecPerBeat).SetEase(PathList[i].Ease);
            }

            if (movingMode == MovingMode.Trigger && i == PathList.Count - 1)
            {
                t.onComplete += () =>
                {
                    seq.Pause();
                    IsMoving = false;
                };
            }

            seq.Append(t);
        }
        seq.SetLoops(-1);
        seq.Pause();
    }

    public override void Trigger()
    {
        switch (movingMode)
        {
            case MovingMode.Trigger:

                if (!IsMoving)
                {
                    IsMoving = true;
                    seq.Restart();
                    seq.Play();
                }
                break;

            case MovingMode.Auto:
                break;
        }

        switch (shrinkMode)
        {
            case ShrinkMode.Trigger:
                StartCoroutine(Shrink());
                break;

            case ShrinkMode.Auto:
                break;
        }
    }

    IEnumerator Shrink()
    {
        if (IsShrinking) yield break;

        Animator.Play("PlatformShrink");
        IsShrinking = true;

        yield return new WaitForSeconds(ShrinkResetBeat * BPM_Counter.instance._beatIntervalSec);
        Expand();
    }
    public void Expand()
    {
        Animator.Play("PlatformExpand");
        IsShrinking = false;
        Collider.enabled = true;
    }


    void ShowActiveEffect()
    {

    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (shrinkMode == ShrinkMode.Auto)
        {
            StartCoroutine(Shrink());
        }
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        PlayerController controller = collision.gameObject.GetComponent<PlayerController>();


        if (controller.IsGrounded == true && controller.PlayerGroundedPoint.position.y > transform.position.y)
        {
            controller.transform.SetParent(transform);
        }
        else
        {
            controller.transform.SetParent(null);
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        collision.collider.transform.SetParent(null);
    }
}
