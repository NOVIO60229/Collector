﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedUpZone : MonoBehaviour, Triggerable
{
    public enum TriggerMode { Trigger, Always };
    public TriggerMode Mode = TriggerMode.Always;

    public float TriggerOnSec = 2;
    private bool IsWorking = false;

    private Vector2 Dir = Vector2.zero;
    public float Speed = 60;

    SpriteRenderer Renderer;
    BoxCollider2D Collider;
    private void Start()
    {
        Collider = GetComponent<BoxCollider2D>();
        Renderer = GetComponent<SpriteRenderer>();
        Init();
    }
    void Init()
    {
        switch (Mode)
        {
            case TriggerMode.Trigger:
                Disable();
                break;
            case TriggerMode.Always:
                Collider.enabled = true;
                break;
        }

        Dir = transform.right;
    }
    public void Trigger()
    {
        if (!IsWorking)
        {
            StartCoroutine(DoTrigger());
        }
    }
    IEnumerator DoTrigger()
    {
        Enable();
        yield return new WaitForSeconds(TriggerOnSec);
        Disable();
    }

    void Enable()
    {
        Collider.enabled = true;
        IsWorking = true;
        Renderer.color += new Color(0, 0, 0, 0.7f);
        ShowActiveEffect();
    }
    void Disable()
    {
        Collider.enabled = false;
        IsWorking = false;
        Renderer.color -= new Color(0, 0, 0, 0.7f);
    }
    void ShowActiveEffect()
    {

    }


    private List<GameObject> ObjList = new List<GameObject>();
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if (!ObjList.Contains(collision.gameObject))
            {
                collision.transform.GetComponent<PlayerStatus>().AddForce(Dir, Speed, 0.8f);
                collision.transform.GetComponent<PlayerController>().ResetJumpTimes();
                ObjList.Add(collision.gameObject);
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            ObjList.Remove(collision.gameObject);
        }
    }


}
