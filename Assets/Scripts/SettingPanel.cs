﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.InputSystem.UI;
using UnityEngine.SceneManagement;

public class SettingPanel : MonoBehaviour
{
    public static SettingPanel Instance;
    public AudioMixer audioMixer;
    public GameObject Panel;
    public InputSystemUIInputModule Module;
    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
    }
    private void Update()
    {
    }
    public void SwitchActive(bool b)
    {
        Panel.SetActive(b);
    }
    public void SetMasterVolume(float volume)
    {
        audioMixer.SetFloat("MasterVolume", volume);
    }
    public void Reload()
    {
        PlayerController.instance.GetComponent<FindSettingPanel>().SwitchToPlayer();
        SceneCutter.Instance.LoadScene(SceneManager.GetActiveScene().name, 2f);
    }
    public void MainMenu()
    {
        PlayerController.instance.GetComponent<FindSettingPanel>().SwitchToPlayer();
        SceneCutter.Instance.LoadScene("Menu", 2f);
    }

    public void Return()
    {
        PlayerController.instance.GetComponent<FindSettingPanel>().SwitchToPlayer();
    }


}
