﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;
using CjLib;

public class BackPack : MonoBehaviour, IDamagable
{
    public GameObject PickUpHint;
    public float PickUpRadius = 1;
    public BoxCollider2D JumpAreaCollider;
    public BoxCollider2D InstanceCollider;
    public SpriteRenderer renderer;

    Rigidbody2D _rigi;
    SpriteRenderer Renderer;
    PlayerController Controller;
    PlayerAnimation Animation;
    private Transform _throwOriginalTransform;
    private Transform _jumpThrowOriginalTransform;
    private float IgnoreCollisionTime;
    private float ThrowForce = 40f;
    private bool IsPickupCD = false;
    private float PickupCDSec = 0.5f;

    public int HP { get; set; }

    //private float _transitionTime = 0.5f;
    private void Start()
    {
        //_collider = GetComponent<BoxCollider2D>();
        _rigi = GetComponent<Rigidbody2D>();
        Renderer = GetComponent<SpriteRenderer>();
        Controller = PlayerController.instance;
        Animation = Controller.GetComponent<PlayerAnimation>();

        IgnoreCollisionTime = Animation._aniSecDic["Throw"] * 0.5f;
    }
    public void SetUp(Transform throwOriginalTransform, Transform jumpThrowOriginalTransform)
    {
        _throwOriginalTransform = throwOriginalTransform;
        _jumpThrowOriginalTransform = jumpThrowOriginalTransform;
    }

    private void Update()
    {
        UpdatePickUpHint();
    }
    bool IsInPickUpRadius()
    {
        return Physics2D.OverlapCircle(transform.position, PickUpRadius, LayerMask.GetMask("Player"));
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, PickUpRadius);
    }
    void UpdatePickUpHint()
    {
        //if (IsInPickUpRadius() && !Controller.IsBackpackOnBack)
        //{
        //    PickUpHint.SetActive(true);
        //}
        //else
        //{
        //    PickUpHint.SetActive(false);
        //}

        if (transform.rotation.eulerAngles.y > 170 && transform.rotation.eulerAngles.y < 190)
        {
            renderer.flipX = true;
        }
        else
        {
            renderer.flipX = false;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            IBounce bounce = collision.transform.GetComponent<IBounce>();
            bounce?.Bounce();
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
    }
    public void Hurt(int damage, Transform attacker)
    {
    }

    public void Trigger()
    {
        if (Controller.IsBackpackOnBack)
        {
            StartCoroutine(Throw());
        }
        else if (IsInPickUpRadius() && !Controller.IsBackpackOnBack && !IsPickupCD)
        {
            PickUp();
        }
    }

    private IEnumerator Throw()
    {

        IsPickupCD = true;
        StartCoroutine(Timer(PickupCDSec, () => IsPickupCD = false));
        Controller.IsBackpackOnBack = false;

        bool IsGrounded = Controller.IsGrounded;
        if (IsGrounded)
        {
            Controller.IsAnimatingThrow = true;
            yield return new WaitForSeconds(Animation._aniSecDic["Throw"] * 0.5f);
            transform.position = _throwOriginalTransform.position;
            transform.rotation = _throwOriginalTransform.rotation;
        }
        else
        {
            Controller.IsAnimatingJumpThrow = true;
            yield return new WaitForSeconds(Animation._aniSecDic["Throw"] * 0.6f);
            transform.position = _jumpThrowOriginalTransform.position;
            transform.rotation = _jumpThrowOriginalTransform.rotation;
        }


        Vector2 throwDir = Controller.FacingDir;
        throwDir += new Vector2(0, Controller.InputDir.y * 0.4f);
        throwDir = throwDir.normalized;

        float force = ThrowForce;
        if (Controller.InputDir.x != 0)
        {
            force += PlayerStatus.BaseRunSpeed;
        }

        IgnoreCollision();
        BackpackShow();
        _rigi.velocity = throwDir * force;

        if (IsGrounded)
            yield return new WaitForSeconds(Animation._aniSecDic["Throw"] * 0.5f);
        else
            yield return new WaitForSeconds(Animation._aniSecDic["Throw"] * 0.4f);

        Controller.IsAnimatingThrow = false;
        Controller.IsAnimatingJumpThrow = false;
    }
    void IgnoreCollision()
    {
        JumpAreaCollider.enabled = false;

        BoxCollider2D playerCollider = Controller.GetComponent<BoxCollider2D>();
        Physics2D.IgnoreCollision(InstanceCollider, playerCollider);
        StartCoroutine(Timer(IgnoreCollisionTime, () =>
        {
            JumpAreaCollider.enabled = true;
            Physics2D.IgnoreCollision(InstanceCollider, playerCollider, false);
        }));
    }
    void BackpackShow()
    {
        _rigi.simulated = true;
        Renderer.enabled = true;
        PickUpHint.SetActive(true);
    }
    void BackpackHide()
    {
        _rigi.simulated = false;
        Renderer.enabled = false;
        PickUpHint.SetActive(false);
    }
    public void PickUp()
    {
        Controller.IsBackpackOnBack = true;
        BackpackHide();
    }

    public IEnumerator Timer(float time, Action EndOperation = null)
    {
        yield return new WaitForSeconds(time);
        EndOperation?.Invoke();
    }
}
