﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSpawnClone : MonoBehaviour
{
    public GameObject Clone;
    private CloneController CloneController;
    void Start()
    {

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            CloneController = Instantiate(Clone).GetComponent<CloneController>();
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            CloneController.Activate(2f);
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            SoundHandler.Instance.PlaySFX("CC", new Vector3(0, 0, 0));
        }
    }
}
