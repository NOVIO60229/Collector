﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
{
    private static T _instance;
    public static T instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject g = new GameObject("new MonoSingleton");
                g.AddComponent<MonoSingleton<T>>();
                print("new mono");
            }
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = (T)this;
    }
    private void Start()
    {
    }
}
