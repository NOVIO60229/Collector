﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabHandler : MonoBehaviour
{
    private static PrefabHandler _instance;
    public static PrefabHandler Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = Instantiate(Resources.Load("PrefabHandler") as GameObject).GetComponent<PrefabHandler>();
            }
            return _instance;
        }
    }

    public GameObject NormalBullet, UpDownBullet, NoRecycleBullet, GravitylBullet;

    public GameObject BackPack;

    public GameObject SceneCutter;

    public GameObject LightingChain;

    public Material WarningBox, WarningCircle;
    public GameObject WarningArea;

    public GameObject ScorePanel;

    public GameObject VerticleLighting;
}

