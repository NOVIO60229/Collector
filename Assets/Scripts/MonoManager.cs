﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MonoManager : MonoSingleton<MonoManager>
{
    private List<Action> updateList = new List<Action>();
    /// <summary>
    /// 防止UpdateList裡的Action對UpdateList的操作導致出錯
    /// </summary>
    private List<Action> tempUpdateList = new List<Action>();

    public static void AddUpdate(Action action)
    {
        instance.addUpdate(action);
    }

    private void addUpdate(Action action)
    {
        if (updateList.Contains(action) == true)
        {
            Debug.LogWarningFormat("{0} 这个Update已经存在", action.ToString());
            return;
        }
        updateList.Add(action);
    }

    public void RemoveUpdate(Action action)
    {
        if (updateList.Contains(action) == false)
        {
            Debug.LogWarningFormat("{0} 这个Update不存在", action.ToString());
            return;
        }
        updateList.Remove(action);
    }

    public void MyUpdate()
    {
        if (updateList.Count == 0) return;

        tempUpdateList.Clear();
        tempUpdateList.AddRange(updateList);
        for (int i = 0; i < tempUpdateList.Count; i++)
        {
            tempUpdateList[i]();
        }
    }
}
