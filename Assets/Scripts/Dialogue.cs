﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialogue : MonoBehaviour
{
    private SpriteRenderer Renderer;

    public Sprite[] welcome;
    public Sprite[] leave;

    public void Start()
    {
        Renderer = GetComponent<SpriteRenderer>();
        ShowRandom(welcome, 1f);
    }
    void ShowRandom(Sprite[] dialogues, float sec)
    {
        StartCoroutine(DoShowRandom(dialogues, sec));
    }
    IEnumerator DoShowRandom(Sprite[] dialogues, float sec)
    {
        int index = Random.Range(0, dialogues.Length);
        Renderer.sprite = dialogues[index];
        Renderer.DOFade(0.7f, sec);

        yield return new WaitForSeconds(2.5f);

        Renderer.DOFade(0, sec);
    }

    public void ShowLeave()
    {
        ShowRandom(leave, 0.4f);
    }
}
