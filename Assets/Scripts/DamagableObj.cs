﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagableObj : MonoBehaviour
{
    public bool IsTrigger = true;
    public bool IsColllder = true;
    public bool IsLethal = false;
    private bool IsClosing;
    public float CloseBeat;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Trigger()
    {
        if (IsClosing) return;
        IsClosing = true;

        if (IsTrigger)
        {

        }
        else if (IsColllder)
        {

        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (IsColllder)
        {
            var d = collision.transform.GetComponent<IDamagable>();

            if (IsLethal && collision.transform.tag == "Player")
            {
                RespawnManager.Instance.Die();
            }
            else
            {
                d?.Hurt(10, transform);
            }
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (IsTrigger)
        {
            var d = collision.transform.GetComponent<IDamagable>();

            if (IsLethal && collision.tag == "Player")
            {
                RespawnManager.Instance.Die();
            }
            else
            {
                d?.Hurt(10, transform);
            }

        }
    }

}
