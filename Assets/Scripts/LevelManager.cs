﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    private static LevelManager _instance;
    public static LevelManager Instance
    {
        get
        {
            if (_instance == null)
            {
                var instance = new GameObject("LevelManager").AddComponent<LevelManager>();
                _instance = instance;
                DontDestroyOnLoad(instance.gameObject);
            }
            return _instance;
        }
    }

    private float FinishTime;
    private float StartTime;
    public void Awake()
    {
        SceneManager.sceneLoaded += TimeStart;
        SceneManager.sceneUnloaded += Clear;
    }
    private void TimeStart(Scene s, LoadSceneMode l)
    {
        StartTime = Time.time;
    }
    private void Clear(Scene s)
    {
        FinishTime = 0;
        StartTime = 0;
    }
    public void Finish()
    {
        TimeEnd();
        PlayerController.instance.GetComponent<PlayerInput>().enabled = false;
        CallScorePanel();
    }
    private void TimeEnd()
    {
        FinishTime = Time.time - StartTime;
    }
    public void CallScorePanel()
    {
        ScorePanel.Instance.ShowScorePanel(FinishTime);
    }


}
