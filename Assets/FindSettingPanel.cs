﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class FindSettingPanel : MonoBehaviour
{
    PlayerInput input;
    private void Start()
    {
        input = GetComponent<PlayerInput>();
        input.uiInputModule = SettingPanel.Instance.Module;
    }
    public void SwitchToSetting()
    {
        SettingPanel.Instance.SwitchActive(true);
        try
        {
            input.SwitchCurrentActionMap("UI");
        }
        catch
        {

        }
    }
    public void SwitchToPlayer()
    {
        SettingPanel.Instance.SwitchActive(false);
        try
        {
            input.SwitchCurrentActionMap("Player");
        }
        catch
        {

        }
    }
}
