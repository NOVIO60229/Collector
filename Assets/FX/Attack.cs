﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Attack : MonoBehaviour
{
    public WarningUtil WarningUtil;
    public LightingUtil LightingUtil;


    public GameObject BOSS;
    private Vector3 startPoint = new Vector3(0.473499f, 0.31869f);
    private Vector3 controllPoint = new Vector3(1, 0);
    private Vector3 endPoint = new Vector3(0, -2.79f);

    Vector3[] b;
    Vector3[] c;
    Vector3[] d;
    private void Start()
    {
        StartCoroutine(Lightning());
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            for (int i = 0; i < 5; i++)
            {
                Vector3 pos = new Vector3(-9f + i * 3, 4.6f, 0);
                Vector3 dir = new Vector3(0, -1, 0);
                WarningUtil.DrawBox(pos, dir, 10f, 1, 0.8f);
            }
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            for (int i = 0; i < 5; i++)
            {
                Vector3 pos = new Vector3(-7.5f + i * 3, 4.6f, 0);
                Vector3 dir = new Vector3(0, -1, 0);
                WarningUtil.DrawBox(pos, dir, 10f, 1, 0.8f);
            }
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            for (int i = 0; i < 5; i++)
            {
                Vector3 pos = new Vector3(-9f + i * 3, 0f, 0);
                Vector3 dir = new Vector3(0, -1, 0);
                WarningUtil.DrawCircle(pos, 2, 2);
            }
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            Vector3[] a = new Vector3[] { new Vector3(-12f, 6.6f), new Vector3(-12f, -6.6f) };
            for (int i = 0; i < 5; i++)
            {
                a[1] += new Vector3(3, 0);
                a[0] += new Vector3(3, 0);

                for (int r = 0; r < 3; r++)
                {
                }
            }
        }

        b = BezierUtils.GetBeizerList(BOSS.transform.position, controllPoint, endPoint, 6);
        c = BezierUtils.GetBeizerList(BOSS.transform.position, controllPoint + new Vector3(4, 0, 0), endPoint, 6);
        d = BezierUtils.GetBeizerList(BOSS.transform.position, controllPoint + new Vector3(-4, 0, 0), endPoint, 6);
    }

    IEnumerator Lightning()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.1f);
        }
    }

    private void OnDrawGizmos()
    {
        foreach (Vector3 dot in b)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(dot, 0.1f);
        }
    }

}
