﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarningUtil : MonoBehaviour
{
    public static WarningUtil Instance;
    private ObjectPool Pool;
    public void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        InitialPool();
    }
    private void InitialPool()
    {
        GameObject g = PrefabHandler.Instance.WarningArea;
        Pool = ObjectPool.CreatPool("WarningAreaPool", g, 10);
    }

    public void DrawBox(Vector3 position, Vector3 dir, float length, float width, float fillSec, Action EndFillAction = null)
    {
        WarningArea w = SpawnWarningArea();
        w.SetWarningArea(position, dir, length, width, fillSec);
        w.SetEndFillAction(() => Pool.Recycle(w.gameObject));
        if (EndFillAction != null)
        {
            w.AddEndFillAction(EndFillAction);
        }
        w.Play();
    }
    public void DrawCircle(Vector3 position, float radius, float fillSec, Action EndFillAction = null)
    {
        WarningArea w = SpawnWarningArea();
        w.SetWarningArea(position, radius, fillSec);
        w.SetEndFillAction(() => Pool.Recycle(w.gameObject));
        w.Play();
    }
    private WarningArea SpawnWarningArea()
    {
        WarningArea w = Pool.Use().GetComponent<WarningArea>();
        return w;
    }
}
