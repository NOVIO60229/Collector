﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarningArea : MonoBehaviour
{
    LineRenderer LineRenderer;
    Action EndFillAction;
    private float FillSec = 1f;

    private void OnEnable()
    {
        LineRenderer = GetComponent<LineRenderer>();
    }
    public void SetWarningArea(Vector3 position, Vector3 dir, float length, float width, float fillSec)
    {
        LineRenderer.material = PrefabHandler.Instance.WarningBox;
        LineRenderer.SetPosition(0, position);
        LineRenderer.SetPosition(1, position + (dir.normalized * length));
        LineRenderer.widthMultiplier = width;
        FillSec = fillSec;
    }
    public void SetWarningArea(Vector3 position, float radius, float fillSec)
    {
        LineRenderer.material = PrefabHandler.Instance.WarningCircle;
        Vector3 distanceToRight = new Vector3(radius, 0, 0);
        LineRenderer.SetPosition(0, position - distanceToRight);
        LineRenderer.SetPosition(1, position + distanceToRight);
        LineRenderer.widthMultiplier = radius * 2;
        FillSec = fillSec;
    }
    public void SetEndFillAction(Action action)
    {
        EndFillAction = null;
        EndFillAction = action;
    }
    public void AddEndFillAction(Action action)
    {
        EndFillAction += action;
    }

    public void Play()
    {
        StartFill();
    }

    private void StartFill()
    {
        StartCoroutine(DoFill());
    }

    private IEnumerator DoFill()
    {
        float fillPercentage = 0;
        while (fillPercentage < 1)
        {
            fillPercentage += (1 / FillSec) * Time.deltaTime;
            fillPercentage = Mathf.Clamp(fillPercentage, 0, 1);
            SetFillPercentage(fillPercentage);
            yield return null;
        }
        EndFillAction.Invoke();
    }
    private void SetFillPercentage(float percent)
    {
        LineRenderer.material.SetFloat("FillPercentage", percent);
    }

}
