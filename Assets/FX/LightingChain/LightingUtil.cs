﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightingUtil : MonoBehaviour
{
    public static LightingUtil Instance;
    private ObjectPool Pool;
    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        InitialPool();
    }
    private void InitialPool()
    {
        GameObject g = PrefabHandler.Instance.LightingChain;
        Pool = ObjectPool.CreatPool("LightingPool", g, 5);
    }
    public void DrawLighting(Vector3[] anchorPoints, int intervalPoints)
    {
        LightingChain lightingChain = SpawnLightingChain();
        lightingChain.SetPointsAndRadius(anchorPoints, intervalPoints);
        lightingChain.SetEndFillAction(() => Pool.Recycle(lightingChain.gameObject));
        lightingChain.Play();
    }
    private LightingChain SpawnLightingChain()
    {
        LightingChain lightingChain = Pool.Use().GetComponent<LightingChain>();
        return lightingChain;
    }
}
