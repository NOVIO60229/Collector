﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(LineRenderer))]
public class LightingChain : MonoBehaviour
{
    private LineRenderer LineRenderer;
    Action EndFillAction;

    public Vector3[] AnchorPoints;
    public int IntervalPoints = 0;
    public float TwistRadius = 0.3f;
    public float Width = 0.15f;
    private float CurrentWidth = 0f;
    public float PersistSec = 0.1f;
    public float FadeSec = 0.1f;

    private bool IsFading = false;

    private void OnEnable()
    {
        LineRenderer = GetComponent<LineRenderer>();
        LineRenderer.widthMultiplier = 0;
    }
    public void SetPointsAndRadius(Vector3[] anchorPoints, int intervalPoints)
    {
        AnchorPoints = anchorPoints;
        IntervalPoints = intervalPoints;
    }
    public void SetEndFillAction(Action action)
    {
        EndFillAction = null;
        EndFillAction = action;
    }
    public void Play()
    {
        StartCoroutine(DoPlay());
    }
    private IEnumerator DoPlay()
    {
        SetPoints();
        LighteningIn();
        yield return new WaitForSeconds(PersistSec);
        LighteningFadeOut();
    }

    private void SetPoints()
    {
        Vector2[] points = GenerateArrayWithIntervalPoints();

        LineRenderer.positionCount = points.Length;
        RandomShiftLightingPathPoint(points);

        for (int i = 0; i < points.Length; i++)
        {
            LineRenderer.SetPosition(i, points[i]);
        }
    }
    private Vector2[] GenerateArrayWithIntervalPoints()
    {
        int totalPoints = AnchorPoints.Length + (AnchorPoints.Length - 1) * IntervalPoints;
        Vector2[] points = new Vector2[totalPoints];

        for (int i = 0; i < AnchorPoints.Length - 1; i++)
        {
            int anchorIndex = i * IntervalPoints + i;
            points[anchorIndex] = AnchorPoints[i];

            Vector3 distancePerInterval = (AnchorPoints[i + 1] - AnchorPoints[i]) / (IntervalPoints + 1);
            for (int k = 1; k <= IntervalPoints; k++)
            {
                int intervalIndex = i * IntervalPoints + i + k;
                points[intervalIndex] = AnchorPoints[i] + distancePerInterval * k;
            }
        }
        points[points.Length - 1] = AnchorPoints[AnchorPoints.Length - 1];

        return points;
    }
    private void RandomShiftLightingPathPoint(Vector2[] points)
    {

        for (int i = 0; i < points.Length; i++)
        {
            if (i == 0 || i == points.Length - 1)
            {
                continue;
            }
            points[i] = points[i] + new Vector2(UnityEngine.Random.Range(-TwistRadius, TwistRadius), UnityEngine.Random.Range(-TwistRadius, TwistRadius));
        }
    }

    private void LighteningIn()
    {
        CurrentWidth = Width;
        LineRenderer.widthMultiplier = CurrentWidth;
    }
    private void LighteningFadeOut()
    {
        if (!IsFading)
        {
            StartCoroutine(DOLighteningFadeOut());
        }
    }
    IEnumerator DOLighteningFadeOut()
    {
        IsFading = true;

        while (CurrentWidth > 0)
        {
            CurrentWidth -= Width * (1 / FadeSec) * Time.deltaTime;
            //CurrentWidth = Mathf.Clamp(CurrentWidth, 0, 999);
            LineRenderer.widthMultiplier = CurrentWidth;
            yield return null;
        }

        IsFading = false;
        EndFillAction?.Invoke();
    }
}
